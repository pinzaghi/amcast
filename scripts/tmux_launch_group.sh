#!/bin/sh 

# https://github.com/greymd/tmux-xpanes

xpanes -ss -c "./scripts/ttynode.sh {} $1 $2 $3 $4 $5" "replica=1" "replica=2" "replica=3"
