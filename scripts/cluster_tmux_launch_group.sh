#!/bin/sh 

# https://github.com/greymd/tmux-xpanes
# $1 = group number
# $2 = cluster node for 1st replica
# $3 = cluster node for 2nd replica
# $4 = cluster node for 3rd replica
# $5 = cluster node for zookeeper
# $6 ... = extra params

xpanes -ss -e "ssh node$2 -t 'bash -l -c \"cd /home/pinzaghi/amcast; ./scripts/ttynode.sh group=$1 replica=1 zoohost=192.168.3.$5 $6 $7 $8\"'" "ssh node$3 -t 'bash -l -c \"cd /home/pinzaghi/amcast; ./scripts/ttynode.sh group=$1 replica=2 zoohost=192.168.3.$5 $6 $7 $8\"'" "ssh node$4 -t 'bash -l -c \"cd /home/pinzaghi/amcast; ./scripts/ttynode.sh group=$1 replica=3 zoohost=192.168.3.$5 $6 $7 $8\"'"
