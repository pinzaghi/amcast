#!/bin/bash

JVM_PATH="-Djava.util.logging.config.file=src/main/resources/logging.properties -cp target/amcast-trunk-jar-with-dependencies.jar"

# GC implementation
JVM_OPTS="-XX:+UseParallelGC -Xverify:none -Xms2G -Xmx2G"

# GC logging options -- uncomment to enable (this is not the exact PID; but close)
#JVM_OPTS="$JVM_OPTS -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:$HOME/$HOSTNAME-$$.vgc"

#JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.port=1098"

# start the program
java $JVM_PATH $JVM_OPTS ar.uba.dc.amcast.lab.TTYNode "$@"
