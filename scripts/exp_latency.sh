#!/bin/bash

JVM_PATH="-Djava.util.logging.config.file=src/main/resources/logging.properties -cp target/amcast-trunk-jar-with-dependencies.jar"
# GC implementation
JVM_OPTS="-XX:+UseParallelGC"
# GC logging options
#JVM_OPTS="$JVM_OPTS -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:$HOME/$HOSTNAME-$$.vgc"
# start the program
java $JVM_PATH -Xms2G -Xmx2G $JVM_OPTS ar.uba.dc.amcast.lab.LatencyExperiment "$@"
