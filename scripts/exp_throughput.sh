#!/bin/bash

JVM_PATH="-Djava.util.logging.config.file=src/main/resources/logging.properties -cp target/amcast-trunk-jar-with-dependencies.jar"
# GC implementation
JVM_OPTS="-XX:+UseParallelGC -Xverify:none -Xms128m"
#JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.port=1098"
# start the program
java $JVM_OPTS $JVM_PATH ar.uba.dc.amcast.lab.ThroughputExperiment "$@"
