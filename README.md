# Practical atomic multicast, a trade-off between genuineness and performance

## Installation

Download and install URingPaxos, full details are found in the repository README.

```
git clone https://github.com/sambenz/URingPaxos.git
cd URingPaxos
mvn -DskipTests install
```

If the last command complains about javadoc erros, run the following:

```
mvn -DskipTests -Dmaven.javadoc.skip=true install
```

In the main directory of the amcast project run:

```
./scripts/build.sh
```

Sometimes is necessary to config URingPaxos library memory type, this is done using the ZooKeeper CLI :

```
/usr/share/zookeeper/bin/zkCli.sh
```

In the CLI run this command, replacing the <group_id>
```
set /ringpaxos/topology<group_id>/config/stable_storage ch.usi.da.paxos.storage.InMemory
```

This need to be done in every process.

## Usage

### Launching a Node

#### Start the ZooKeeper service

```
/etc/init.d/zookeeper start
```

Or with:

```
./zookeeper-3.4.12/bin/zkServer.sh start
```

#### Launch the node

To launch a node for a given group you can use the ttynode.sh script:

```
./scripts/ttynode.sh group=<group_id> replica=<node_id> zoohost=<zookeeper_host>
```

For example, this will launch a node #2 in a group #1 with a local ZooKeeper server running on port 2181:

```
./scripts/ttynode.sh group=1 replica=2 zoohost=127.0.0.1:2181
```

Everything will be ready when the shell informs the following:

```
INFO  Coordinator succesfully trimmed acceptor log to instance 0
```

This shell will inform about new client messages requests, consensus and other useful information.

