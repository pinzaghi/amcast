package ar.uba.dc.amcast;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Test;

import ar.uba.dc.amcast.treecast.OverlayTreeNode;

public class TestOverlayTree {
	
	@Test
	public void testSeed() {
		
		OverlayTreeNode<Integer> tree = new OverlayTreeNode<Integer>(2);
		
		Set<Integer> nodesToCover = new LinkedHashSet<Integer>();
		nodesToCover.add(2);
		
		assertEquals(tree,tree.lowerCommonAncestor(nodesToCover));
		nodesToCover.clear();
		
		nodesToCover.add(1);
		
		assertEquals(null,tree.lowerCommonAncestor(nodesToCover));
		
	}
	
	@Test
	public void testLowerCommonAncestorOneLevel() {
		
		OverlayTreeNode<Integer> node1;
		OverlayTreeNode<Integer> node3;
		OverlayTreeNode<Integer> node4;
		
		OverlayTreeNode<Integer> tree = new OverlayTreeNode<Integer>(2);
		{
			node1 = tree.addChild(1);
			node3 = tree.addChild(3);
			node4 = tree.addChild(4);
		}
		
		Set<Integer> nodesToCover = new LinkedHashSet<Integer>();
		nodesToCover.add(2);
		
		assertEquals(tree,tree.lowerCommonAncestor(nodesToCover));
		nodesToCover.clear();
		
		// 2 is the parent of 1 and 4 so is the lca
		nodesToCover.add(1);
		nodesToCover.add(4);
		assertEquals(tree,tree.lowerCommonAncestor(nodesToCover));
		nodesToCover.clear();
		
		// 2 is the parent of 1 but is the only child covered, we can make it better
		nodesToCover.add(1);
		assertEquals(node1,tree.lowerCommonAncestor(nodesToCover));
		nodesToCover.clear();
		
		// Node 5 doesn't exist so null is expected
		nodesToCover.add(5);
		assertEquals(null,tree.lowerCommonAncestor(nodesToCover));
		nodesToCover.clear();
		
	}
	
	@Test
	public void testLowerCommonAncestorMoreLevels() {
		
		OverlayTreeNode<Integer> node1;
		OverlayTreeNode<Integer> node2;
		OverlayTreeNode<Integer> node3;
		OverlayTreeNode<Integer> node4;
		OverlayTreeNode<Integer> node5;
		OverlayTreeNode<Integer> node6;
		OverlayTreeNode<Integer> node7;
		
		node1 = new OverlayTreeNode<Integer>(1);
		{
			node2 = node1.addChild(2);
			{
				node2.addChild(4);
				node2.addChild(5);
			}
			node3 = node1.addChild(3);
			{
				node3.addChild(6);
				node3.addChild(7);
			}
			
		}
		
		// Worst case
		Set<Integer> nodesToCover = new LinkedHashSet<Integer>();
		nodesToCover.add(5);
		nodesToCover.add(6);
		
		assertEquals(node1,node1.lowerCommonAncestor(nodesToCover));
		nodesToCover.clear();
		
		// Not so bad
		nodesToCover.add(4);
		nodesToCover.add(5);
		
		assertEquals(node2,node1.lowerCommonAncestor(nodesToCover));
		nodesToCover.clear();
		
		// Another bad one
		nodesToCover.add(7);
		nodesToCover.add(2);
		
		assertEquals(node1,node1.lowerCommonAncestor(nodesToCover));
		nodesToCover.clear();
		
	}
	
	@Test
	public void testReach() {
		OverlayTreeNode<Integer> node2;
		OverlayTreeNode<Integer> node3;
		
		OverlayTreeNode<Integer> tree = new OverlayTreeNode<Integer>(1);
		{
			node2 = tree.addChild(2);
			node3 = tree.addChild(3);
		}
		LinkedHashSet<Integer> set = new LinkedHashSet<Integer>();
		set.add(2);
		set.add(3);
		
		assertTrue(tree.reach(set));
		assertTrue(node2.reach(set));
		assertTrue(node3.reach(set));
		
	}
	
	@Test
	public void testReachBinary7() {
		
		OverlayTreeNode<Integer> node1;
		OverlayTreeNode<Integer> node2;
		OverlayTreeNode<Integer> node3;
		OverlayTreeNode<Integer> node4;
		OverlayTreeNode<Integer> node5;
		OverlayTreeNode<Integer> node6;
		OverlayTreeNode<Integer> node7;
		
		node1 = new OverlayTreeNode<Integer>(1);
		{
			node2 = node1.addChild(2);
			{
				node4 = node2.addChild(4);
				node5 = node2.addChild(5);
			}
			node3 = node1.addChild(3);
			{
				node6 = node3.addChild(6);
				node7 = node3.addChild(7);
			}
			
		}
		
		LinkedHashSet<Integer> set56 = new LinkedHashSet<Integer>();
		set56.add(5);
		set56.add(6);
		
		LinkedHashSet<Integer> set7 = new LinkedHashSet<Integer>();
		set7.add(7);
		
		assertTrue(node1.reach(set56));
		assertTrue(node2.reach(set56));
		assertTrue(node3.reach(set56));
		assertFalse(node4.reach(set56));
		assertTrue(node5.reach(set56));
		assertTrue(node6.reach(set56));
		assertFalse(node7.reach(set56));
		
		assertTrue(node1.reach(set7));
		assertTrue(node3.reach(set7));
		assertTrue(node7.reach(set7));
		assertFalse(node2.reach(set7));
		assertFalse(node4.reach(set7));
		assertFalse(node5.reach(set7));
		
		
	}
	

}
