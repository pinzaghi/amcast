package ar.uba.dc.amcast;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import org.apache.zookeeper.KeeperException;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import ar.uba.dc.amcast.api.AmcastClient;
import ar.uba.dc.amcast.communication.io.ClientCommunication;
import ar.uba.dc.amcast.treecast.OverlayTreeNode;

public class TestAmulticast {
	
	/*
	 * 
	 * IMPORTANT
	 * 
	 * To run this tests, the nodes must be launched with the Writer watcher.
	 * Is also very important to clean the generated files by the watcher (e.g. amcast_delivered_n1_g1.log)
	 * in every run of the test
	 * 
	 * ./scripts/tmux_launch_group.sh 1 127.0.0.1:2181 writer
	 */
	static String zoo_host = "127.0.0.1:2181";

	int group_1 = 0;
	int group_2 = 1;
	int group_3 = 2;
	int group_4 = 3;
	
	Vector<Vector<LinkedList<String>>> deliveredMessages;
	
	Gson gson = new GsonBuilder().create();
	
	public class DummyClient implements Runnable{
		
		public List<String> messagesToSend;
		public LinkedHashMap<String, LinkedHashSet<Integer>> destinations;
		public AmcastClient client;

		public DummyClient(AmcastClient client, List<String> messagesToSend, LinkedHashMap<String, LinkedHashSet<Integer>> destinations) {
			this.messagesToSend = messagesToSend;
			this.destinations = destinations;
			this.client = client;
		}

		@Override
		public void run() {
			
			System.out.println(client.toString() + " starts to send " + messagesToSend.size()+ " messages");
			int i = 1;
			for(String m : messagesToSend) {
				System.out.println(client+" start sending message "+m);
				try {
					client.amulticast(m.getBytes("UTF-8"), destinations.get(m));
				} catch (InterruptedException | IOException e) {
					e.printStackTrace();
				}
				System.out.println(client+" message # "+i+"/"+messagesToSend.size());
				System.out.println(client+" finish sending message "+m);
				i++;
			}
			System.out.println(client.toString() + " sent " + messagesToSend.size()+ " messages");
		}

	}


	@BeforeClass
	public static void prepare() throws Exception {
		
	}
	
	@Before 
	public void initialize() throws Exception {
		deliveredMessages = new Vector<Vector<LinkedList<String>>>();
				
		for(int i=0; i<=3 ; i++) {
			deliveredMessages.add(i,  new Vector<LinkedList<String>>());
			for(int j=0; j<=2 ; j++) {
				deliveredMessages.get(i).add(j,  new LinkedList<String>());
			}
		}	
	}

	public void readDeliveredMessages(int groupQty) {
		
		for(int i=0; i<groupQty ; i++) {
			for(int j=0; j<=2 ; j++) {
				
				FileReader fr = null;
				BufferedReader br;
				String sCurrentLine;
				
				try {
					fr = new FileReader(System.getProperty("user.home")+"/amcast_delivered_n"+(j+1)+"_g"+(i+1)+".log");
					br = new BufferedReader(fr);
					
					while ((sCurrentLine = br.readLine()) != null) {
						deliveredMessages.get(i).get(j).add(sCurrentLine);
					}
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (JsonSyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}
	
	@Test
	public void TreecastOneClientGlobalMessagesTwoGroups() throws InterruptedException, IOException, KeeperException {
		
		ClientCommunication communication = new ClientCommunication(zoo_host);
		
		OverlayTreeNode<Integer> overlayTree = OverlayTreeNode.getSampleTree("3parent_1_2");
		
		Client client_1234 = new Client(1234, communication, overlayTree);

		client_1234.connectToLocalReplicas(3);
		
		oneClientGlobalMessagesTwoGroups(client_1234);
	}
	
	@Test
	public void BasecastOneClientGlobalMessagesTwoGroups() throws InterruptedException, IOException, KeeperException {
		
		ClientCommunication communication = new ClientCommunication(zoo_host);
		LinkedHashSet<Integer> groups = new LinkedHashSet<Integer>();
		groups.add(1);
		groups.add(2);
		
		Client client_1234 = new Client(1234, communication, groups);

		client_1234.connectToLocalReplicas(1);
		
		oneClientGlobalMessagesTwoGroups(client_1234);
	}
	
	public void oneClientGlobalMessagesTwoGroups(Client client_1234) throws InterruptedException, IOException, KeeperException {
		
		List<String> client_1234_messagesToSend = new LinkedList<String>();
		LinkedHashSet<Integer> dst = new LinkedHashSet<Integer>();
		dst.add(1);
		dst.add(2);
		
		LinkedHashMap<String, LinkedHashSet<Integer>> destinations = new LinkedHashMap<String, LinkedHashSet<Integer>>();

		for(int number=0; number<=300 ; number++) {
			String m = ("client 1234 global "+String.valueOf(number));
			client_1234_messagesToSend.add(m);
			destinations.put(m, dst);
		}
		Collections.shuffle(client_1234_messagesToSend, new Random(1));
		
		Thread t1 = new Thread(
				new DummyClient(client_1234, client_1234_messagesToSend, destinations)
        );
		t1.start();
		t1.join();
		
		// Wait for all the nodes to receive the last message
		Thread.sleep(1000);
		
		readDeliveredMessages(2);
		
		// All members of each group have the same messages in the same order
		assertEquals(deliveredMessages.get(group_1).get(0),deliveredMessages.get(group_1).get(1));
		assertEquals(deliveredMessages.get(group_1).get(1),deliveredMessages.get(group_1).get(2));
		// ...and comparing to the original list of messages they are equals as a set (disordered)
		assertEquals(new LinkedHashSet<String>(client_1234_messagesToSend), new LinkedHashSet<String>(deliveredMessages.get(group_1).get(2)));
		
		// All members of each group have the same messages in the same order
		assertEquals(deliveredMessages.get(group_2).get(0),deliveredMessages.get(group_2).get(1));
		assertEquals(deliveredMessages.get(group_2).get(1),deliveredMessages.get(group_2).get(2));
		// ...and comparing to the original list of messages they are equals as a set (disordered)
		assertEquals(new LinkedHashSet<String>(client_1234_messagesToSend), new LinkedHashSet<String>(deliveredMessages.get(group_2).get(2)));

	}
	
	@Test
	public void TreecastTwoClientsLocalMessagesTwoGroups() throws InterruptedException, IOException, KeeperException {
		
		ClientCommunication communication1 = new ClientCommunication(zoo_host);
		ClientCommunication communication2 = new ClientCommunication(zoo_host);
		
		OverlayTreeNode<Integer> overlayTree = OverlayTreeNode.getSampleTree("3parent_1_2");
		
		Client client_1234 = new Client(1234, communication1, overlayTree);
		Client client_1235 = new Client(1235, communication2, overlayTree);

		client_1234.connectToLocalReplicas(3);
		client_1235.connectToLocalReplicas(1);
		
		twoClientsLocalMessagesTwoGroups(client_1234, client_1235);
	}
	
	@Test
	public void BasecastTwoClientsLocalMessagesTwoGroups() throws InterruptedException, IOException, KeeperException {
		
		ClientCommunication communication1 = new ClientCommunication(zoo_host);
		ClientCommunication communication2 = new ClientCommunication(zoo_host);
		LinkedHashSet<Integer> groups = new LinkedHashSet<Integer>();
		groups.add(1);
		groups.add(2);
		
		Client client_1234 = new Client(1234, communication1, groups);
		Client client_1235 = new Client(1235, communication2, groups);

		client_1234.connectToLocalReplicas(1);
		
		twoClientsLocalMessagesTwoGroups(client_1234, client_1235);
	}
	
	public void twoClientsLocalMessagesTwoGroups(Client client_1234, Client client_1235) throws InterruptedException, IOException, KeeperException {
		
		// Messages to send to Group 1
		List<String> client_1234_messagesToSend = new LinkedList<String>();
		LinkedHashSet<Integer> dst1 = new LinkedHashSet<Integer>();
		dst1.add(1);
		LinkedHashMap<String, LinkedHashSet<Integer>> destinations = new LinkedHashMap<String, LinkedHashSet<Integer>>();
		
		// Messages to send to Group 2
		List<String> client_1235_messagesToSend = new LinkedList<String>();
		LinkedHashSet<Integer> dst2 = new LinkedHashSet<Integer>();
		dst2.add(2);
		
		List<String> messages_group1 = new LinkedList<String>();
		List<String> messages_group2 = new LinkedList<String>();
		
		for(int number=0; number<=30 ; number++) {
			String msg1 = ("client 1234 local 1 "+String.valueOf(number));
			String msg2 = ("client 1234 local 2 "+String.valueOf(number));
			
			client_1234_messagesToSend.add(msg1);
			destinations.put(msg1, dst1);
			client_1234_messagesToSend.add(msg2);
			destinations.put(msg2, dst2);
			
			messages_group1.add(msg1);
			messages_group2.add(msg2);
		}
		Collections.shuffle(client_1234_messagesToSend, new Random(2));
		
		for(int number=0; number<=30 ; number++) {
			String msg1 = ("client 1235 local 1 "+String.valueOf(number));
			String msg2 = ("client 1235 local 2 "+String.valueOf(number));
			
			client_1235_messagesToSend.add(msg1);
			destinations.put(msg1, dst1);
			client_1235_messagesToSend.add(msg2);
			destinations.put(msg2, dst2);
			
			messages_group1.add(msg1);
			messages_group2.add(msg2);
		}
		Collections.shuffle(client_1235_messagesToSend, new Random(2));
		
		
		Thread t1 = new Thread(
				new DummyClient(client_1234, client_1234_messagesToSend, destinations)
        );
		t1.start();
		
		Thread t2 = new Thread(
				new DummyClient(client_1235, client_1235_messagesToSend, destinations)
        );
		t2.start();
		
		t1.join();
		t2.join();
		
		// Wait for all the nodes to receive the last message
		Thread.sleep(1000);
		
		readDeliveredMessages(2);
		
		// All members of each group have the same messages in the same order
		assertEquals(deliveredMessages.get(group_1).get(0),deliveredMessages.get(group_1).get(1));
		assertEquals(deliveredMessages.get(group_1).get(1),deliveredMessages.get(group_1).get(2));
		assertEquals(new LinkedHashSet<String>(messages_group1), new LinkedHashSet<String>(deliveredMessages.get(group_1).get(2)));
		
		assertEquals(deliveredMessages.get(group_2).get(0),deliveredMessages.get(group_2).get(1));
		assertEquals(deliveredMessages.get(group_2).get(1),deliveredMessages.get(group_2).get(2));
		assertEquals(new LinkedHashSet<String>(messages_group2), new LinkedHashSet<String>(deliveredMessages.get(group_2).get(2)));

	}
	
	
	@Test
	public void TreecastTwoClientsGlobalAndLocalMessagesTwoGroups() throws InterruptedException, IOException, KeeperException {
		
		ClientCommunication communication1 = new ClientCommunication(zoo_host);
		ClientCommunication communication2 = new ClientCommunication(zoo_host);
		
		OverlayTreeNode<Integer> overlayTree = OverlayTreeNode.getSampleTree("3parent_1_2");
		
		Client client_1234 = new Client(1234, communication1, overlayTree);
		Client client_1235 = new Client(1235, communication2, overlayTree);

		client_1234.connectToLocalReplicas(3);
		client_1235.connectToLocalReplicas(1);
		
		twoClientsGlobalAndLocalMessagesTwoGroups(client_1234, client_1235);
	}
	
	@Test
	public void BasecastTwoClientsGlobalAndLocalMessagesTwoGroups() throws InterruptedException, IOException, KeeperException {
		
		ClientCommunication communication1 = new ClientCommunication(zoo_host);
		ClientCommunication communication2 = new ClientCommunication(zoo_host);
		LinkedHashSet<Integer> groups = new LinkedHashSet<Integer>();
		groups.add(1);
		groups.add(2);
		
		Client client_1234 = new Client(1234, communication1, groups);
		Client client_1235 = new Client(1235, communication2, groups);

		client_1234.connectToLocalReplicas(1);
		
		twoClientsGlobalAndLocalMessagesTwoGroups(client_1234, client_1235);
	}
	
	public void twoClientsGlobalAndLocalMessagesTwoGroups(Client client_1234, Client client_1235) throws InterruptedException, IOException, KeeperException {

		LinkedHashMap<String, LinkedHashSet<Integer>> destinations = new LinkedHashMap<String, LinkedHashSet<Integer>>();
		
		LinkedHashSet<Integer> dstGlobal = new LinkedHashSet<Integer>();
		dstGlobal.add(1);
		dstGlobal.add(2);
		
		LinkedHashSet<Integer> dst1 = new LinkedHashSet<Integer>();
		dst1.add(1);
		
		LinkedHashSet<Integer> dst2 = new LinkedHashSet<Integer>();
		dst2.add(2);
		
		List<String> client_1234_global_messages = new LinkedList<String>();
		List<String> client_1235_global_messages = new LinkedList<String>();
		List<String> client_1234_messages = new LinkedList<String>();
		List<String> client_1235_messages = new LinkedList<String>();
		
		for(int number=0; number<=300 ; number++) {
			String msg1 = "client 1234 local "+String.valueOf(number);
			String msg2 = "client 1234 global "+String.valueOf(number);
			
			client_1234_messages.add(msg1);
			destinations.put(msg1, dst1);
			client_1234_global_messages.add(msg2);
			destinations.put(msg2, dstGlobal);
		}
		
		for(int number=0; number<=300 ; number++) {
			String msg1 = "client 1235 local "+String.valueOf(number);
			String msg2 = "client 1235 global "+String.valueOf(number);
			
			client_1235_messages.add(msg1);
			destinations.put(msg1, dst2);
			client_1235_global_messages.add(msg2);
			destinations.put(msg2, dstGlobal);
		}
		
		
		// Two clients sending both local and global messages
		List<String> client_1234_local_and_global = new LinkedList<String>(client_1234_messages);
		client_1234_local_and_global.addAll(client_1234_global_messages);
		Collections.shuffle(client_1234_local_and_global, new Random(1));
		
		Thread t1 = new Thread(
				new DummyClient(client_1234, client_1234_local_and_global, destinations )
        );
		t1.start();
		
		List<String> client_1235_local_and_global = new LinkedList<String>(client_1235_messages);
		client_1235_local_and_global.addAll(client_1235_global_messages);
		Collections.shuffle(client_1235_local_and_global, new Random(1));
		
		Thread t2 = new Thread(
				new DummyClient(client_1235, client_1235_local_and_global, destinations)
        );
		t2.start();
		
		t1.join();
		t2.join();
		
		// Wait for all the nodes to receive the last message
		Thread.sleep(1000);
		
		readDeliveredMessages(2);
		
		// Group 1
		// All nodes has the original list of messages equals as a set (disordered)
		LinkedHashSet<String> resulting_messages_group1 = new LinkedHashSet<String>(client_1234_messages);
		resulting_messages_group1.addAll(client_1234_global_messages);
		resulting_messages_group1.addAll(client_1235_global_messages);
		assertEquals(resulting_messages_group1, new LinkedHashSet<String>(deliveredMessages.get(group_1).get(2)));
		// ...and nodes of each group have the same messages in the same order
		assertEquals(deliveredMessages.get(group_1).get(0),deliveredMessages.get(group_1).get(1));
		assertEquals(deliveredMessages.get(group_1).get(1),deliveredMessages.get(group_1).get(2));
		
		// Group 2
		// All nodes has the original list of messages equals as a set (disordered)
		LinkedHashSet<String> resulting_messages_group2 = new LinkedHashSet<String>(client_1235_messages);
		resulting_messages_group2.addAll(client_1234_global_messages);
		resulting_messages_group2.addAll(client_1235_global_messages);
		assertEquals(resulting_messages_group2, new LinkedHashSet<String>(deliveredMessages.get(group_2).get(0)));
		// All nodes of each group have the same messages in the same order
		assertEquals(deliveredMessages.get(group_2).get(0),deliveredMessages.get(group_2).get(1));
		assertEquals(deliveredMessages.get(group_2).get(1),deliveredMessages.get(group_2).get(2));
		
		// Between groups 1 and 2, shared messages (global) must be in the same relative order
		LinkedList<String> globalMessagesGroup1 = new LinkedList<String>(deliveredMessages.get(group_1).get(0));
		globalMessagesGroup1.removeAll(client_1234_messages);
		
		LinkedList<String> globalMessagesGroup2 = new LinkedList<String>(deliveredMessages.get(group_2).get(0));
		globalMessagesGroup2.removeAll(client_1235_messages);
		
		assertEquals(globalMessagesGroup1,globalMessagesGroup2);

	}
	
	@Test
	public void TreecastFourClientsGlobalAndLocalMessagesFourGroups() throws InterruptedException, IOException, KeeperException {
		
		OverlayTreeNode<Integer> overlayTree = OverlayTreeNode.getSampleTree("1parent_2_3_4");
		
		ClientCommunication communication;
		Client[] clients = new Client[4];
		
		for(int client_id = 0; client_id < 4; client_id++) {
			communication = new ClientCommunication(zoo_host);
			clients[client_id] = new Client(client_id+1, communication, overlayTree);
			for(int group_id=1; group_id < 5; group_id++) {
				clients[client_id].connectToLocalReplicas(1);
			}
		}
		
		fourClientsGlobalAndLocalMessagesFourGroups(clients);
	}
	
	@Test
	public void BasecastFourClientsGlobalAndLocalMessagesFourGroups() throws InterruptedException, IOException, KeeperException {
		
		ClientCommunication communication;
		Client[] clients = new Client[4];
		
		LinkedHashSet<Integer> groups = new LinkedHashSet<Integer>();
		groups.add(1);
		groups.add(2);
		groups.add(3);
		groups.add(4);
		
		for(int client_id = 0; client_id < 4; client_id++) {
			communication = new ClientCommunication(zoo_host);
			clients[client_id] = new Client(client_id+1, communication, groups);
			for(int group_id=1; group_id < 5; group_id++) {
				clients[client_id].connectToLocalReplicas(1);
			}
		}
		
		fourClientsGlobalAndLocalMessagesFourGroups(clients);
	}
	
	public void fourClientsGlobalAndLocalMessagesFourGroups(Client[] clients) throws InterruptedException, IOException, KeeperException {

		LinkedHashMap<String, LinkedHashSet<Integer>>[] client_sent_dsts = new LinkedHashMap[4];
		LinkedList<String>[] client_sent_messages = new LinkedList[4];
		for(int client_id = 0; client_id < 4; client_id++) {
			client_sent_dsts[client_id] = new LinkedHashMap<String, LinkedHashSet<Integer>>();
			client_sent_messages[client_id] = new LinkedList<String>();
		}

		LinkedHashSet<Integer> dst1 = new LinkedHashSet<Integer>();
		dst1.add(1);
		
		LinkedHashSet<Integer> dst2 = new LinkedHashSet<Integer>();
		dst2.add(2);
		
		LinkedHashSet<Integer> dst1_2_3 = new LinkedHashSet<Integer>();
		dst1_2_3.add(1);dst1_2_3.add(2);dst1_2_3.add(3);
		
		LinkedHashSet<Integer> dst_1_2_3_4 = new LinkedHashSet<Integer>();
		dst_1_2_3_4.add(1);dst_1_2_3_4.add(2);dst_1_2_3_4.add(3);dst_1_2_3_4.add(4);

		
		for(int client_id = 0; client_id < 4; client_id++) {
			for(int number=0; number<=300 ; number++) {
				LinkedHashSet<Integer> dst = null;
				
				if(client_id == 0) {
					dst = dst1;
				}else if(client_id == 1) {
					dst = dst2;
				}else if(client_id == 2) {
					dst = dst1_2_3;
				}else if(client_id == 3) {
					dst = dst_1_2_3_4;
				}
				
				String msg = "client "+(client_id+1)+" dst "+dst+" "+String.valueOf(number);
				
				client_sent_messages[client_id].add(msg);
				client_sent_dsts[client_id].put(msg, dst);
				
			}
		}
		
		Collections.shuffle(client_sent_messages[0], new Random(1));
		Collections.shuffle(client_sent_messages[1], new Random(1));
		Collections.shuffle(client_sent_messages[2], new Random(1));
		Collections.shuffle(client_sent_messages[3], new Random(1));
		
		Thread[] threads = new Thread[4];
		
		for(int client_id = 0; client_id < 4; client_id++) {
			threads[client_id] = new Thread(new DummyClient(clients[client_id], client_sent_messages[client_id], client_sent_dsts[client_id] ));
			threads[client_id].start();
		}
		
		for(int client_id = 0; client_id < 4; client_id++) {
			threads[client_id].join();
		}

		// Wait for all the nodes to receive the last message
		Thread.sleep(1000);
		
		readDeliveredMessages(4);
		
		// All nodes within the same group has the original list of messages equals as a set
		for(int group_id = 0; group_id<4; group_id++) {
			LinkedHashSet<String> resulting_messages_group = new LinkedHashSet<String>();

			if(group_id == 0) {
				resulting_messages_group.addAll(client_sent_messages[0]);
				resulting_messages_group.addAll(client_sent_messages[2]);
				resulting_messages_group.addAll(client_sent_messages[3]);
			}else if(group_id == 1) {
				resulting_messages_group.addAll(client_sent_messages[1]);
				resulting_messages_group.addAll(client_sent_messages[2]);
				resulting_messages_group.addAll(client_sent_messages[3]);
			}else if(group_id == 2) {
				resulting_messages_group.addAll(client_sent_messages[2]);
				resulting_messages_group.addAll(client_sent_messages[3]);
			}else if(group_id == 3) {
				resulting_messages_group.addAll(client_sent_messages[3]);
			}
			
			assertEquals(resulting_messages_group, new LinkedHashSet<String>(deliveredMessages.get(group_id).get(2)));
			// ...and nodes of each group have the same messages in the same order
			assertEquals(deliveredMessages.get(group_id).get(0),deliveredMessages.get(group_id).get(1));
			assertEquals(deliveredMessages.get(group_id).get(1),deliveredMessages.get(group_id).get(2));
		}
		
		// Groups 1, 2 and 3 all have messages from Client #3 in the same order
		LinkedList<String> expected_messages = new LinkedList<String>();
		expected_messages.addAll(client_sent_messages[2]);

		LinkedList<String> globalMessagesGroup1 = new LinkedList<String>(deliveredMessages.get(group_1).get(0));
		globalMessagesGroup1.removeAll(client_sent_messages[0]); // Remove messages from client #1
		globalMessagesGroup1.removeAll(client_sent_messages[3]); // Remove messages from client #4
		
		assertEquals(expected_messages,globalMessagesGroup1);
		
		LinkedList<String> globalMessagesGroup2 = new LinkedList<String>(deliveredMessages.get(group_2).get(0));
		globalMessagesGroup2.removeAll(client_sent_messages[1]); // Remove messages from client #2
		globalMessagesGroup2.removeAll(client_sent_messages[3]); // Remove messages from client #4
		
		assertEquals(expected_messages,globalMessagesGroup2);
		
		LinkedList<String> globalMessagesGroup3 = new LinkedList<String>(deliveredMessages.get(group_3).get(0));
		globalMessagesGroup3.removeAll(client_sent_messages[3]); // Remove messages from client #4
		
		assertEquals(expected_messages,globalMessagesGroup3);
		
		// Groups 1, 2, 3 and 4 all have messages from Client #4 in the same order
		expected_messages = new LinkedList<String>();
		expected_messages.addAll(client_sent_messages[3]);

		globalMessagesGroup1 = new LinkedList<String>(deliveredMessages.get(group_1).get(0));
		globalMessagesGroup1.removeAll(client_sent_messages[0]); // Remove messages from client #1
		globalMessagesGroup1.removeAll(client_sent_messages[2]); // Remove messages from client #3
		
		assertEquals(expected_messages,globalMessagesGroup1);
		
		globalMessagesGroup2 = new LinkedList<String>(deliveredMessages.get(group_2).get(0));
		globalMessagesGroup2.removeAll(client_sent_messages[1]); // Remove messages from client #2
		globalMessagesGroup2.removeAll(client_sent_messages[2]); // Remove messages from client #4
		
		assertEquals(expected_messages,globalMessagesGroup2);
		
		globalMessagesGroup3 = new LinkedList<String>(deliveredMessages.get(group_3).get(0));
		globalMessagesGroup3.removeAll(client_sent_messages[2]); // Remove messages from client #4
		
		assertEquals(expected_messages,globalMessagesGroup3);
		
		LinkedList<String> globalMessagesGroup4 = new LinkedList<String>(deliveredMessages.get(group_4).get(0));
		
		assertEquals(expected_messages,globalMessagesGroup4);
		
	}
	
	
	@After
	public void close() throws InterruptedException {
	}
}
