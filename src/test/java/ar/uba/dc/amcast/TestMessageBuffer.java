package ar.uba.dc.amcast;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.MessageType;

public class TestMessageBuffer {
	
	@Test
	public void serializeMessage() throws Exception {
		LinkedHashSet<Integer> dst = new LinkedHashSet<Integer>();
		dst.add(1);
		dst.add(2);
		dst.add(3);
		Message m = new Message(  MessageType.ClientMessageDelivered, 
								  dst, 
								  3, 
								  40L, 
								  "test".getBytes(), 
								  "1234567");
		
		String wire = "00000006"+ // ClientMessageDelivered
					  "00000003"+ // 3 dsts
					  "00000001"+
					  "00000002"+
					  "00000003"+
					  "00000003"+ // origin group
					  "0000000000000028"+ // timestamp = 40_(16)
					  "00000007"+ // id size
					  "31323334353637"+ // 123456 ascii
					  "00000004"+
					  "74657374"; // test
		
		assertEquals(wire.length()/2,Message.length(m));
		assertEquals(wire,bytesToHex(Message.toWire(m)) );	
		

		assertEquals(m,Message.fromWire(Message.toWire(m)));
		
		m = new Message(  MessageType.ClientMessageDelivered, null, null, null, null, "1234567");
		assertEquals(m,Message.fromWire(Message.toWire(m)));
		
		 wire =   "00000006"+ // ClientMessageDelivered
				  "FFFFFFFF"+ // 
				  "FFFFFFFF"+ // origin group
				  "FFFFFFFFFFFFFFFF"+ // timestamp = 40_(16)
				  "00000007"+ // id size
				  "31323334353637"+ // 1234567 ascii
				  "00000004"+
				  "74657374"; // test
		
		m = new Message(  MessageType.ClientMessageDelivered, null, null, null, "test".getBytes(), "1234567");
		
		assertEquals(wire,bytesToHex(Message.toWire(m)) );	
		
		assertEquals(bytesToHex(m.getValue()), bytesToHex(Message.fromWire(Message.toWire(m)).getValue()));
		
		assertEquals( m, Message.fromWire(Message.toWire(m)) );
		assertTrue(Arrays.equals(m.getValue(), Message.fromWire( Message.toWire(m) ).getValue() ) );
		
	}
	
	@Test
	public void serializeMessages() throws Exception {
		
		LinkedHashSet<Integer> dst = new LinkedHashSet<Integer>();
		dst.add(1);
		dst.add(2);
		dst.add(3);
		
		LinkedHashSet<Integer> dst2 = new LinkedHashSet<Integer>();
		dst2.add(1);

		
		Message m1 = new Message(  MessageType.ClientMessageDelivered, 
				  dst, 
				  3, 
				  40L, 
				  "test".getBytes(), 
				  "1");
		
		Message m2= new Message(  MessageType.ClientMessageDelivered, 
				  dst2, 
				  1, 
				  42L, 
				  "test".getBytes(), 
				  "123");
		
		Message m3 = new Message(  MessageType.ClientMessageDelivered, 
				  dst, 
				  2, 
				  43L, 
				  "test".getBytes(), 
				  "123456");
		
		LinkedList<Message> messages = new LinkedList<Message>();
		messages.add(m1);
		messages.add(m2);
		messages.add(m3);
		
		assertEquals( messages, Message.listFromWire(Message.listToWire(messages)) );
		
	}
	
	@Test
	public void serializeListOfLists() throws Exception {
		
		LinkedHashSet<Integer> dst = new LinkedHashSet<Integer>();
		dst.add(1);
		dst.add(2);
		dst.add(3);
		
		LinkedHashSet<Integer> dst2 = new LinkedHashSet<Integer>();
		dst2.add(1);

		
		Message m1 = new Message(  MessageType.ClientMessageDelivered, 
				  dst, 
				  3, 
				  40L, 
				  "test".getBytes(), 
				  "1");
		
		Message m2= new Message(  MessageType.ClientMessageDelivered, 
				  dst2, 
				  1, 
				  42L, 
				  "test".getBytes(), 
				  "123");
		
		Message m3 = new Message(  MessageType.ClientMessageDelivered, 
				  dst, 
				  2, 
				  43L, 
				  "test".getBytes(), 
				  "123456");
		
		LinkedList<Message> messages = new LinkedList<Message>();
		messages.add(m1);
		messages.add(m2);
		messages.add(m3);
		
		List<List<Message>> list = new LinkedList<List<Message>>();
		list.add(new LinkedList<Message>());
		list.add(messages);
		list.add(messages);
		
		assertEquals( list, Message.listOfListsFromWire(Message.listOfListsToWire(list)) );
		
	}
	
	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}


}
