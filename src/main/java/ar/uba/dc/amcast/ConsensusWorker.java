package ar.uba.dc.amcast;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import ar.uba.dc.amcast.api.AmcastNode;
import ar.uba.dc.amcast.api.Consensus;
import ar.uba.dc.amcast.message.Message;
import ch.usi.da.paxos.storage.Decision;

public class ConsensusWorker implements Runnable {
		
	private static final Logger LOGGER = Logger.getLogger( ConsensusWorker.class.getName() );
	
	private int proposalBatchSize = 50;
	
	private Consensus consensus;
	private AmcastNode node;
	protected boolean running = false;
	BlockingQueue<List<Message>> decisionQueue;
	BlockingQueue<List<Message>> proposalsQueue;

	public ConsensusWorker(Consensus consensus, AmcastNode node, Integer proposalBatchSize) {
		this.node = node;
		this.consensus = consensus;
		if(proposalBatchSize != null) {
			this.proposalBatchSize = proposalBatchSize;
		}
		this.decisionQueue = new LinkedBlockingQueue<List<Message>>();
		this.proposalsQueue = new LinkedBlockingQueue<List<Message>>(this.proposalBatchSize);

		LOGGER.log( Level.INFO,"ConsensusWorker batch size {0}",new Object[] {this.proposalBatchSize});
	}
	
	public void run() {
		
		synchronized(this){
            this.running = true;
        }
		
		//LOGGER.log( Level.FINE, node+" launching decision watcher");
		
		BlockingQueue<Decision> decideInterface = consensus.getDecisions();
		Decision d;
		List<List<Message>> decisions = null;
		
		Thread tdw = new Thread(){
			public void run() {
			  while(true) {
					List<Message> decision = null;
					try {
						decision = decisionQueue.take();
					} catch (InterruptedException e) {
						StringWriter sw = new StringWriter();
				    	PrintWriter pw = new PrintWriter(sw);
				    	e.printStackTrace(pw);
				    	
				    	LOGGER.log(Level.SEVERE, "Exception {0} {1}", new Object[] {e, sw.toString()});
					}
					node.handleDecision(decision);
				}
			}
		}; 
		tdw.setName("Decision worker");
		tdw.start();
		
		Thread tpw = new Thread(){
			public void run() {
			  while(true) {
				  
				  	try {
						List<List<Message>> messages = new LinkedList<List<Message>>();
						messages.add(proposalsQueue.take());
						proposalsQueue.drainTo(messages);

						consensus.propose(Message.listOfListsToWire(messages));
						
						LOGGER.log( Level.FINE, "Proposed {0} elements: {1}", new Object[] {messages.size(), messages});
						
					} catch (InterruptedException e) {
						StringWriter sw = new StringWriter();
				    	PrintWriter pw = new PrintWriter(sw);
				    	e.printStackTrace(pw);
				    	
				    	LOGGER.log(Level.SEVERE, "Exception {0} {1}", new Object[] {e, sw.toString()});
					}

				}
			}
		}; 
		tpw.setName("Proposal worker");
		tpw.start();
		
		while(isRunning()){
			
			try {
				//LOGGER.log( Level.FINE, node+" waiting for new decision");
				// Block waiting for a new decision
				d = decideInterface.take();
				decisions = Message.listOfListsFromWire(d.getValue().getValue());
				for(List<Message> decision : decisions) {
					decisionQueue.put(decision);
				}
				
				LOGGER.log( Level.FINE,"New decision {0}",new Object[] {decisions});

				//LOGGER.log( Level.FINE, node+" learning new decision "+decision+" timestamp, "+System.nanoTime());
				
				
			} catch (InterruptedException e) {
				StringWriter sw = new StringWriter();
		    	PrintWriter pw = new PrintWriter(sw);
		    	e.printStackTrace(pw);
		    	
		    	LOGGER.log(Level.SEVERE, "Exception {0} {1}", new Object[] {e, sw.toString()});
			}
		
		}
		
	}
	
	public synchronized boolean isRunning() {
        return this.running;
    }

	public synchronized void stop() {
		this.running = false;
	}

	public void propose(List<Message> messages) {
		try {
			proposalsQueue.put(messages);			
		} catch (InterruptedException e) {
			StringWriter sw = new StringWriter();
	    	PrintWriter pw = new PrintWriter(sw);
	    	e.printStackTrace(pw);
	    	
	    	LOGGER.log(Level.SEVERE, "Exception {0} {1}", new Object[] {e, sw.toString()});
		}
	}
}
