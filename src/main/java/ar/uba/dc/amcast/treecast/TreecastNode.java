package ar.uba.dc.amcast.treecast;

import java.net.SocketAddress;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import ar.uba.dc.amcast.Node;
import ar.uba.dc.amcast.api.AmcastServerCommunication;
import ar.uba.dc.amcast.api.AmcastWatcher;
import ar.uba.dc.amcast.api.Consensus;
import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.MessageType;

public class TreecastNode extends Node {
	
	private OverlayTreeNode<Integer> overlayTree;
	private List<OverlayTreeNode<Integer>> children;
	// (MesaggeId , GroupId) -> confirmationReceived
	private Map<String, Map<Integer, Boolean>> receivedMessageDeliveryConfirmation;

	public TreecastNode(OverlayTreeNode<Integer> overlayTree, Consensus consensus, AmcastServerCommunication network, Integer nodeId, Integer groupId, Integer proposalBatchSize,
			AmcastWatcher watcher) {
		super(consensus, network, nodeId, groupId, watcher, proposalBatchSize);
		
		this.overlayTree = overlayTree;
		this.receivedMessageDeliveryConfirmation = new ConcurrentHashMap<String, Map<Integer,Boolean>>();
		
		for(OverlayTreeNode<Integer> node : this.overlayTree) {
			if(node.data.equals(getGroupId())) {
				children = node.children;
			}
		}
		
		LOGGER.log(Level.INFO, "Node g:{0} r:{1} Children {2}", new Object[] {groupId,nodeId,children});
	}

	public synchronized void receiveFromCommunication(Message message, SocketAddress from) {
		
		LOGGER.log(Level.FINE, "Node g:{0} r:{1} Received a message {2}", new Object[] {groupId,nodeId,message});

		if(this.isCoordinator()) {
			
			if(message.getType() == MessageType.ClientStart || message.getType() == MessageType.Start) {
				
				LOGGER.log(Level.FINER, "Node g:{0} r:{1} Received a client start {2}", new Object[] {groupId,nodeId,message});
				
				message.setType(MessageType.Start);
				
				// We register who sent this message to reply back on delivery
				messageOwnerSocket.put(message.getCacheId(), from);
				receivedMessageDeliveryConfirmation.put(message.getCacheId(), new LinkedHashMap<Integer,Boolean>());
				
				for(OverlayTreeNode<Integer> childNode : children) {
					if(childNode.reach(message.getDestinations())) {
						Integer childGroupId = childNode.data;
						receivedMessageDeliveryConfirmation.get(message.getCacheId()).put(childGroupId, false);
					}
				}

				List<Message> messages = new LinkedList<Message>();
				messages.add(message);

				proposeMessages(messages);

			}
			
			if(message.getType() == MessageType.ClientMessageDelivered) {
				
				LOGGER.log(Level.FINER, "Node g:{0} r:{1} Received a delivery confirm {2}", new Object[] {groupId,nodeId,message});
	
				Map<Integer, Boolean> messageReceived = receivedMessageDeliveryConfirmation.get(message.getCacheId());
				Integer originGroup = message.getOriginGroup();
				messageReceived.put(originGroup, true);
				
				Boolean allConfirmationsReceived = true;
				Set<Integer> groupsWaitingForConfirm = receivedMessageDeliveryConfirmation.get(message.getCacheId()).keySet();
				for(Boolean received : receivedMessageDeliveryConfirmation.get(message.getCacheId()).values()) {
					if(!received) {
						allConfirmationsReceived = false;
						break;
					}
				}
				
				if(allConfirmationsReceived) {
					LOGGER.log(Level.FINER, "Received all confirmations {0} of delivery for message {1}", new Object[] {groupsWaitingForConfirm, message});
					confirmDeliveryToOwner(message);
					receivedMessageDeliveryConfirmation.remove(message.getCacheId());
				}
			}

		}else{
			// We forward the message to the group coordinator
			if(message.getType() == MessageType.ClientStart) {
				LOGGER.log(Level.FINER, "Message {0} forwarded to coordinator {1}", new Object[] {message, getCoordinatorID()});
				messageOwnerSocket.put(message.getCacheId(), from);
				message.setType(MessageType.Start);
				sendMessageToCoordinator(message);
			// We forward the confirmation to the client
			}else if(message.getType() == MessageType.ClientMessageDelivered) {
				confirmDeliveryToOwner(message);
			}
		}
		 
	}

	public synchronized void handleDecision(List<Message> decided) {
		
		LOGGER.log(Level.FINER, "Node g:{0} r:{1} Handling decision {2}", new Object[] {groupId,nodeId,decided});
		
		for(Message m : decided) {
			
			LOGGER.log(Level.FINER, "Decided message {2}", new Object[] {groupId,nodeId,m});
			
			// I'm in the destination so we deliver the message
			if(m.getDestinations().contains(getGroupId())) {
				deliverMessage(m);
			}
						
			if(this.isCoordinator()) {
				
				// If I'm in the destinations I count my delivery 
				if(m.getDestinations().contains(getGroupId())) {
					receivedMessageDeliveryConfirmation.get(m.getCacheId()).put(getGroupId(), true);
				}
				
				Boolean lowestDestinationOnBranch = true;
				
				for(OverlayTreeNode<Integer> childNode : children) {
					if(childNode.reach(m.getDestinations())) {
						lowestDestinationOnBranch = false;
						Integer childGroupId = childNode.data;
						LOGGER.log(Level.FINER, "Children group {0} reach some of the destinations for {1}", new Object[] {childGroupId, m});
						sendMessageToGroup(m, childGroupId);
					}
				}
				
				// I'm the lower destination on this branch for message, I can confirm delivery to parent
				if(lowestDestinationOnBranch) {
					LOGGER.log(Level.FINER, "I am the lower destination on this branch for message {0}", new Object[] {m});
					confirmDeliveryToOwner(m);
					messageOwnerRemove(m);
				}
			}
			
		}
		
	}

	public void deliverMessage(Message m) {
		LOGGER.log(Level.FINE, "Deliver message {0}", new Object[] {m});
		
		if(watcher != null) {
			watcher.adeliver(m);
		}

	}
	

}
