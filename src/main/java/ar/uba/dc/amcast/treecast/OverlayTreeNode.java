package ar.uba.dc.amcast.treecast;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class OverlayTreeNode<T> implements Iterable<OverlayTreeNode<T>> {

	public T data;
	public OverlayTreeNode<T> parent;
	public List<OverlayTreeNode<T>> children;

	public boolean isRoot() {
		return parent == null;
	}

	public boolean isLeaf() {
		return children.size() == 0;
	}

	private List<OverlayTreeNode<T>> elementsIndex;

	public OverlayTreeNode(T data) {
		this.data = data;
		this.children = new LinkedList<OverlayTreeNode<T>>();
		this.elementsIndex = new LinkedList<OverlayTreeNode<T>>();
		this.elementsIndex.add(this);
	}

	public OverlayTreeNode<T> addChild(T child) {
		OverlayTreeNode<T> childNode = new OverlayTreeNode<T>(child);
		childNode.parent = this;
		this.children.add(childNode);
		this.registerChildForSearch(childNode);
		return childNode;
	}

	public int getLevel() {
		if (this.isRoot())
			return 0;
		else
			return parent.getLevel() + 1;
	}

	private void registerChildForSearch(OverlayTreeNode<T> node) {
		elementsIndex.add(node);
		if (parent != null)
			parent.registerChildForSearch(node);
	}

	public OverlayTreeNode<T> findTreeNode(Comparable<T> cmp) {
		for (OverlayTreeNode<T> element : this.elementsIndex) {
			T elData = element.data;
			if (cmp.compareTo(elData) == 0)
				return element;
		}

		return null;
	}

	@Override
	public String toString() {
		return data != null ? data.toString() : "[data null]";
	}

	@Override
	public Iterator<OverlayTreeNode<T>> iterator() {
		OverlayTreeNodeIter<T> iter = new OverlayTreeNodeIter<T>(this);
		return iter;
	}
	
	public OverlayTreeNode<T> lowerCommonAncestor(Set<T> nodes) {
		OverlayTreeNode<T> lca = null;
		
		// I am one of the destinations, I'm the lca
		if(nodes.contains(data)) {
			lca = this;
		}else {
			if(!isLeaf()) {
				List<OverlayTreeNode<T>> childrenCovered = new LinkedList<OverlayTreeNode<T>>();
				
				for(OverlayTreeNode<T> child : children) {
					if(child.lowerCommonAncestor(nodes) != null) {
						childrenCovered.add(child);
					}
				}
				
				// Nothing to do, we didn't find any node
				if(childrenCovered.size() == 0) {
					lca = null;
				// I can go down more in the tree	
				}else if(childrenCovered.size() == 1) {
					lca = childrenCovered.get(0).lowerCommonAncestor(nodes);
				// I'm the lca because I cover more than 1 node
				}else {
					lca = this;
				}
			}
		}
		return lca;
		
	}
	
	// If any of my descendants are in the elements set return true
	public boolean reach(Set<T> elements) {
		
		Boolean reachable = false;
		
		if(elements.contains(data)) {
			reachable = true;
		}else if(children.size() == 0) {
			reachable = false;
		}else {
			
			for(OverlayTreeNode<T> node : children) {
				if(node.reach(elements)) {
					reachable = true;
					break;
				}
			}
		
		}
		
		return reachable;
		
	}
	
	public static OverlayTreeNode<Integer> getSampleTree(String tree_option){
		
		OverlayTreeNode<Integer> tree = null;
		
		if(tree_option.equals("1group")) {
			tree = new OverlayTreeNode<Integer>(1);
		}
		
		
		/*
		 * 		1
		 * 	   / \
		 *    2   3
		 */	  	
		if(tree_option.equals("1parent_2_3")) {
			tree = new OverlayTreeNode<Integer>(1);
			{
				tree.addChild(2);
				tree.addChild(3);
			}
		}
		
		/*
		 * 		3
		 * 	   / \
		 *    1   2
		 */	  	
		if(tree_option.equals("3parent_1_2")) {
			tree = new OverlayTreeNode<Integer>(3);
			{
				tree.addChild(1);
				tree.addChild(2);
			}
		}
		
		/*
		 * 		  1
		 * 	   /  |  \
		 *    2   3  4
		 */	  	
		if(tree_option.equals("1parent_4")) {
			tree = new OverlayTreeNode<Integer>(1);
			{
				tree.addChild(2);
				tree.addChild(3);
				tree.addChild(4);
			}
		}
		
		/*
		 * 		 1
		 * 	    / \
		 *     2   3
		 *    /     \
		 *   4       5
		 *  /		  \
		 * 6           7
		 * 
		 */	  	
		if(tree_option.equals("bad_for_6_7")) {
			tree = new OverlayTreeNode<Integer>(1);
			{
				OverlayTreeNode<Integer> node2 = tree.addChild(2);
				{
					OverlayTreeNode<Integer> node4 = node2.addChild(4);
					{
						node4.addChild(6);
					}
				}
				OverlayTreeNode<Integer> node3 = tree.addChild(3);
				{
					OverlayTreeNode<Integer> node5 = node3.addChild(5);
					{
						node5.addChild(7);
					}
				}
			}
		}
		
		/*
		 * 		   1
		 * 	   /  ...  \
		 *    2 3 4 5 6 7
		 */	  	
		if(tree_option.equals("1parent_7")) {
			tree = new OverlayTreeNode<Integer>(1);
			{
				tree.addChild(2);
				tree.addChild(3);
				tree.addChild(4);
				tree.addChild(5);
				tree.addChild(6);
				tree.addChild(7);
			}
		}
		
		/*
		 * 		  1
		 * 	    /   \
		 *     2     3
		 *    / \   / \
		 *   4   5 6   7
		 *  		  
		 *
		 * 
		 */	  	
		if(tree_option.equals("binary_balanced_7")) {
			tree = new OverlayTreeNode<Integer>(1);
			{
				OverlayTreeNode<Integer> node2 = tree.addChild(2);
				{
					node2.addChild(4);
					node2.addChild(5);
				}
				OverlayTreeNode<Integer> node3 = tree.addChild(3);
				{
					node3.addChild(6);
					node3.addChild(7);
				}
			}
		}
		
		
		return tree;
	}

}
