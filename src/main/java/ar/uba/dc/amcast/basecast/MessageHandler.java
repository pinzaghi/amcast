package ar.uba.dc.amcast.basecast;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import ar.uba.dc.amcast.api.AmcastMessageHandler;
import ar.uba.dc.amcast.api.AmcastNode;
import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.MessageType;

public class MessageHandler implements AmcastMessageHandler {
	
	private static final Logger LOGGER = Logger.getLogger( MessageHandler.class.getName() );
	
	private BasecastNode node;
	
	public List<Message> toOrder; // toOrder
	public Map<Long, List<Message>> proposedMessages;
	public Set<Message> ordered; // ordered MessageId
	public Set<Message> tentativeTimestamps; // beta
	
	// Aux structure for speed
	public Map<String, Set<Message>> tentativeTimestamps_syncHard;
	public Map<Message,Long> tentativeTimestamps_finalMessages;
	
	public Map<String, Set<Message>> receivedSendHards;
	
	public Long proposeInstance, decideInstance; // k_p, k_d
	public Long logicalClock; // C_h
	
	
	public MessageHandler(BasecastNode node) {
		
		this.node = node;
		this.toOrder = new LinkedList<Message>();
		this.proposedMessages = new LinkedHashMap<Long, List<Message>>();
		this.ordered = new LinkedHashSet<Message>();
		this.tentativeTimestamps = new HashSet<Message>();
		// MessageId -> Groups
		this.tentativeTimestamps_syncHard = new LinkedHashMap<String, Set<Message>>();
		this.tentativeTimestamps_finalMessages = new HashMap<Message,Long>();
		
		this.receivedSendHards = new LinkedHashMap<String, Set<Message>>();
		
		this.proposeInstance = 0L;
		this.decideInstance = 0L;
		this.logicalClock = 0L;

	}
	
	// Task1 and Task2
	public void handleMessage(Message deliveredMessage) {

		LOGGER.log( Level.FINE, "Handle {0}", new Object[]{deliveredMessage} );
		
		if(deliveredMessage.getType() == MessageType.Start) {
			handleStartMessage(deliveredMessage);
		}else if(deliveredMessage.getType() == MessageType.SendHard) {
			handleSendHardMessage(deliveredMessage);
		}
	
	}
	
	private void handleStartMessage(Message deliveredMessage){
		
		Message setHardMessage = new Message(	MessageType.SetHard,
												deliveredMessage.getDestinations(),
												node.getGroupId(), 
												null, 
												deliveredMessage.getValue(),
												deliveredMessage.getCacheId());
		addTo_toOrder(setHardMessage);
		proposeMessagesToOrder();
		
	}
	
	private void handleSendHardMessage(Message deliveredMessage) {
		LOGGER.log( Level.FINE, "Task2: handle SEND-HARD message");
		
		registerSendHardMessage(deliveredMessage);
	}
	
	private synchronized void registerSendHardMessage(Message deliveredMessage) {
		
		if(!receivedSendHards.containsKey(deliveredMessage.getCacheId())) {
			receivedSendHards.put(deliveredMessage.getCacheId(), new HashSet<Message>());
		}
		receivedSendHards.get(deliveredMessage.getCacheId()).add(deliveredMessage);
		
		// We received all SendHard messages, we order the SyncHard messages
		if(receivedSendHards.get(deliveredMessage.getCacheId()).size() == deliveredMessage.getDestinations().size()) {
			
			LOGGER.log( Level.FINE, "I collected all the SendHard messages, we add SyncHards to ToOrder");
			
			for(Message sendHardMessage : receivedSendHards.get(deliveredMessage.getCacheId())) {
				Message syncHardMessage = new Message(MessageType.SyncHard,
																sendHardMessage.getDestinations(), 
																sendHardMessage.getOriginGroup(), 
																sendHardMessage.getTimestamp(),
																null,
																sendHardMessage.getCacheId());
			
				addTo_toOrder(syncHardMessage);
				proposeMessagesToOrder();
			}
		}else {
			LOGGER.log( Level.FINE, "SendHards so far {0}", new Object[]{receivedSendHards.get(deliveredMessage.getCacheId())});
		}
	}
	
	// Task 3
	private synchronized void proposeMessagesToOrder() {
		
		// We filter yet ordered messages
		List<Message> notYetOrderedMessages = getNotYetOrderedMessages();
		
		LOGGER.log( Level.FINER, "Task3: ToOrder \\ Ordered = {0}", new Object[]{notYetOrderedMessages});
		
		// When ToOrder \ Ordered =\= \EmptySet
		if( notYetOrderedMessages.size() > 0 ) {
			
			LOGGER.log( Level.FINER, "Task3: propose_{0}", new Object[]{node.getGroupId()+"["+proposeInstance+"]("+notYetOrderedMessages+")"});
			
			proposedMessages.put(proposeInstance, notYetOrderedMessages);
			proposeInstance++;
			
			// Only the coordinator propose
			if(node.isCoordinator()) {
				node.proposeMessages(notYetOrderedMessages);
			}
			
			toOrder = new LinkedList<Message>();

		}
		
	}

	// ToOrder \ Ordered
	private List<Message> getNotYetOrderedMessages(){
		
		List<Message> notYetOrderedMessages;
		Set<Message> proposedLimbo = new LinkedHashSet<Message>();

		if(proposeInstance-decideInstance>0) {
			for(Long instance : proposedMessages.keySet()) {
				proposedLimbo.addAll(proposedMessages.get(instance));
			}
		}

		LOGGER.log( Level.FINER, "Messages in last proposal without decision {0}", new Object[]{proposedLimbo} );
		
		// We don't want to propose already ordered messages or messages in the proposing "limbo"
		notYetOrderedMessages = toOrder.stream().filter(toOrderMsg -> ordered.stream().noneMatch(ordm -> ordm.equals(toOrderMsg))
																	  && !proposedLimbo.contains(toOrderMsg))
												.collect(Collectors.toList());

		
		return notYetOrderedMessages;
	}
	
	// Task 4
	public synchronized void handleDecision(List<Message> decidedMessages) {

		LOGGER.log( Level.FINER, "launched Task4: decide_{0}", new Object[]{node.getGroupId()+"["+decideInstance+"]("+decidedMessages+")"});
		
		for(Message decidedMsg : decidedMessages) {

			LOGGER.log( Level.FINE,"handleDecision {0}", decidedMsg.getCacheId());
			// Decided \ Ordered
			if(!inOrdered(decidedMsg)) {
				
				if(decidedMsg.getType() == MessageType.SetHard) {
					
					logicalClock++;
					LOGGER.log( Level.FINER, "Task4: logicalClock++ ");
					
					if(decidedMsg.getDestinations().size() > 1) {
						Message sendHardMessage = new Message(MessageType.SendHard, 
																			decidedMsg.getDestinations(),
																			node.getGroupId(), 
																			logicalClock, 
																			null,
																			decidedMsg.getCacheId());
						
						addTo_tentativeTimestamps(sendHardMessage);

						registerSendHardMessage(sendHardMessage);

						if(node.isCoordinator()) {
							LOGGER.log( Level.FINER, "Multicast {0}",sendHardMessage);
							node.multicast(sendHardMessage);
						}
						
					}else {
						Message syncHardMessage = new Message(MessageType.SyncHard, 
																			decidedMsg.getDestinations(),
																			node.getGroupId(), 
																			logicalClock, 
																			null,
																			decidedMsg.getCacheId());
						
						addTo_tentativeTimestamps(syncHardMessage);
					}
					
				}else if(decidedMsg.getType() == MessageType.SyncHard) {
					
					logicalClock = Math.max(logicalClock, decidedMsg.getTimestamp());
					LOGGER.log( Level.FINER, "Task4: logicalClock ← Max({logicalClock, {0} })", decidedMsg.getTimestamp());
					
					Message sendHardMessage = new Message(MessageType.SendHard,
																		decidedMsg.getDestinations(), 
																		decidedMsg.getOriginGroup(), 
																		decidedMsg.getTimestamp(),
																		null,
																		decidedMsg.getCacheId());
					removeFrom_tentativeTimestamps(sendHardMessage);
					
					Message syncHardMessage = new Message(MessageType.SyncHard, 
																		decidedMsg.getDestinations(),
																		decidedMsg.getOriginGroup(), 
																		decidedMsg.getTimestamp(), 
																		null,
																		decidedMsg.getCacheId());
					addTo_tentativeTimestamps(syncHardMessage);
					
				}
				
				addTo_ordered(decidedMsg);
				//removeFrom_toOrder(decidedMsg);
				
			}else {
				LOGGER.log( Level.FINER, "Task4: skipping decided msg {0}", decidedMsg);
			}

		}

		// Not decided messages must be proposed again
		List<Message> proposedNotYetDecided = getProposal(decideInstance);
		proposedNotYetDecided.removeIf( pm -> decidedMessages.stream().anyMatch(dm -> pm.equals(dm)));
		
		if(proposedNotYetDecided.size() > 0) {
			addMessagesTo_toOrder(proposedNotYetDecided);
			proposeMessagesToOrder();
		}
		
		proposedMessages = new LinkedHashMap<Long, List<Message>>();
		decideInstance++;
		
		replaceTentativeTimestamps();
		deliverMessages();
		
	}
	
	// Task 5
	private void deliverMessages() {
		
		LOGGER.log( Level.FINE, "deliverMessages() start");
		
		LinkedList<Message> deliverableMessages = getDeliverableMessages();
		
		LOGGER.log( Level.FINE, "Checking if they are deliverableMessage");
		
		for(Message m : deliverableMessages) {
			
    		LOGGER.log( Level.FINE, "ADELIVER message {0} to client", m);

    		receivedSendHards.remove(m.getCacheId());
    		ordered.removeIf( ordMsg -> ordMsg.getCacheId().equals(m.getCacheId()) );
    		tentativeTimestamps.removeIf( ttMessage -> ttMessage.getCacheId().equals(m.getCacheId()) );
    		
			node.deliverMessage(m);

		}

	}
	
	private void replaceTentativeTimestamps() {

		/**
		 * Lines 34-35
		 * 
		 * Replace m’s tentative timestamps in B by m’s final timestamp
		 * tentativeTimestamps_finalMessages = { m : ∀h ∈ m.dst ∃x : (SYNC-HARD, h, x, m) ∈ tentativeTimestamps }
		 */
		
		LOGGER.log( Level.FINER, "Replace m’s tentative timestamps in B by m’s final timestamp START: {0}", tentativeTimestamps);
		
		// for each z ,h, x : (z ,h, x ,m) ∈ B do B ← B \ {(z ,h, x ,m)}
		tentativeTimestamps.removeIf( ttm -> tentativeTimestamps_finalMessages.keySet().stream().anyMatch(
		   									 fm -> ttm.getCacheId().equals(fm.getCacheId())
		));
		
		for(Entry<Message, Long> finalMessageEntry : tentativeTimestamps_finalMessages.entrySet()) {
			Message fm = new Message(	MessageType.Final,
													finalMessageEntry.getKey().getDestinations(),
													null, 
													finalMessageEntry.getValue(),
													null,
													finalMessageEntry.getKey().getCacheId());
			
			addTo_tentativeTimestamps(fm);
		}
		tentativeTimestamps_finalMessages = new HashMap<Message,Long>();

		LOGGER.log( Level.FINER, "Replace m’s tentative timestamps in B by m’s final timestamp");
	}
	
	private synchronized LinkedList<Message> getDeliverableMessages() {		
		/*
		 * Lines 36-38
		 * 
		 * For each deliverable message, deliver it!
		 */
		LinkedList<Message> deliverableMessages = new LinkedList<Message>();
		
		LinkedList<Message> orderedFinalMessages = new LinkedList<Message>();
		for(Message ttm : tentativeTimestamps) {
			if(ttm.getTimestamp() != null) {
				orderedFinalMessages.add(ttm);
			}
		}
		Collections.sort(orderedFinalMessages, (s1, s2) -> { return s1.occurredEarlierThan(s2) ? -1 
																	: s2.occurredEarlierThan(s1) ? 1 : 0; });
		
		LOGGER.log( Level.FINER, "Task5: orderedFinalMessages {0}", orderedFinalMessages);
		
		Long currentTimestamp = 0L;
	    for (Message m : orderedFinalMessages) {
	    	// We found a non deliverable message with a minor ts
	    	if(m.getType() != MessageType.Final) {
	    		break;
	    	}
	        if (deliverableMessages.isEmpty() || currentTimestamp == m.getTimestamp()-1) {
	        	currentTimestamp = m.getTimestamp();
	        	deliverableMessages.add(m);
	        } else {
	        	break;
	        }
	    }
		
		LOGGER.log( Level.FINER, "Task5: deliverableMessages {0}", deliverableMessages);
		
		return deliverableMessages;
		
	}
	
	private List<Message> getProposal(Long instance) {
		
		List<Message> proposed;

		if(proposedMessages.get(instance) != null) {
			proposed =  new LinkedList<Message>(proposedMessages.get(instance));
		}else {
			proposed =  new LinkedList<Message>();
		}
		
		LOGGER.log( Level.FINER, "I proposed {0} on {1} instance", new Object[] {proposed, instance});
		
		return proposed;
		
	}
	
	private boolean inOrdered(Message message) {
		boolean inOrdered;

		inOrdered = ordered.contains(message);

		LOGGER.log( Level.FINER, "{0} \\in ordered ? {1}", new Object[] {message, inOrdered});
		
		return inOrdered;
	}
	
	private void addTo_tentativeTimestamps(Message message) {
		LOGGER.log( Level.FINER, "tentativeTimestamps ← tentativeTimestamps ∪ {0} ", message);

		if(message.getType() == MessageType.SyncHard) {
			if(!tentativeTimestamps_syncHard.containsKey(message.getCacheId())) {
				tentativeTimestamps_syncHard.put(message.getCacheId(), new LinkedHashSet<Message>());
			}
			Set<Message> messageSyncHards = tentativeTimestamps_syncHard.get(message.getCacheId());
			messageSyncHards.add(message);
			
			// Get the origin groups of all the synchard messages, if I collect all the destinations...
			Set<Integer> dsts = messageSyncHards.stream().map(Message::getOriginGroup).collect(Collectors.toSet());
			
			if(dsts.equals(message.getDestinations())) {
				List<Long> timestamps = messageSyncHards.stream()
										              	.map(Message::getTimestamp)
										              	.collect(Collectors.toList());
				// ... I can calculate the final ts
				Long ts = Collections.max(timestamps);
				
				tentativeTimestamps_finalMessages.put(new Message(message), ts);
				tentativeTimestamps_syncHard.remove(message.getCacheId());
			}
		}
		
		tentativeTimestamps.add(message);
		
	}
	
	private void removeFrom_tentativeTimestamps(Message message) {
		LOGGER.log( Level.FINER, "tentativeTimestamps ← tentativeTimestamps \\ {0}",message);
		tentativeTimestamps.remove(message);
		
	}
	
	private void addTo_ordered(Message message) {
		LOGGER.log( Level.FINER,"ordered ← ordered + {0}",message);
		ordered.add(message);
	}
	
	private synchronized void addTo_toOrder(Message message) {
		LOGGER.log( Level.FINER,"toOrder ← toOrder + {0}",message);
		toOrder.add(message);
	}

	private void addMessagesTo_toOrder(List<Message> messages) {
		LOGGER.log( Level.FINER,"toOrder ← toOrder ∪ {0}",messages);
		toOrder.addAll(messages);
	}

}
