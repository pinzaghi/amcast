package ar.uba.dc.amcast.basecast;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.DataCache;
import ar.uba.dc.amcast.Node;
import ar.uba.dc.amcast.ConsensusWorker;
import ar.uba.dc.amcast.api.AmcastNode;
import ar.uba.dc.amcast.api.AmcastServerCommunication;
import ar.uba.dc.amcast.api.AmcastWatcher;
import ar.uba.dc.amcast.api.Consensus;
import ar.uba.dc.amcast.communication.ZookeeperUtils;
import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.MessageType;

public class BasecastNode extends Node {

	private MessageHandler messageHandler;
	protected DataCache messageCache;

	public BasecastNode(Consensus consensus, AmcastServerCommunication network, Integer nodeId, Integer groupId, Integer proposal_batch) throws IOException, KeeperException, InterruptedException {
		this(consensus, network, nodeId, groupId, proposal_batch, null);
	}
	
	public BasecastNode(Consensus consensus, AmcastServerCommunication network, Integer nodeId, Integer groupId, Integer proposal_batch, AmcastWatcher watcher) throws IOException, KeeperException, InterruptedException {
		super(consensus, network, nodeId, groupId, watcher, proposal_batch);
		this.messageHandler = new MessageHandler(this);
		this.messageCache = new DataCache();
	}
	
	public void handleDecision(List<Message> messages) {
		LOGGER.log(Level.FINER, "Decided {0} time {1}", new Object[] {messages,System.nanoTime()});
		for(Message m : messages) {
			if(m.getType() == MessageType.SetHard) {
				messageCache.store(m.getCacheId(), m.getValue());
			}
		}
		messageHandler.handleDecision(messages);
	}
	
	public void receiveFromCommunication(Message deliveredMessage, SocketAddress from) {
		
		// A client requested to amulticast a message
		if(deliveredMessage.getType() == MessageType.ClientStart) {
			messageCache.store(deliveredMessage.getCacheId(), deliveredMessage.getValue());
			messageOwnerSocket.put(deliveredMessage.getCacheId(), from);
			
			//BasecastNode.log( this, Level.FINER, " Creating start message ");
			Message cm = new Message(	MessageType.Start, 
													deliveredMessage.getDestinations(),
													null,
													null,
													deliveredMessage.getValue(),
													deliveredMessage.getCacheId());
			//BasecastNode.log( this, Level.FINER, " Comm Multicast "+cm);
			multicast(cm);
			messageHandler.handleMessage(cm);
		// Add a tuple to be ordered by consensus
		}else {
			messageHandler.handleMessage(deliveredMessage);
		}
		
	}
	
	public void multicast(Message m) {
    	//ServerCommunication.log( this, Level.FINE, "multicast"+m);
    	if(isRunning()) {
    		for(Integer groupId : m.getDestinations()){
    			sendMessageToGroup(m, groupId);
    		}
    	}
	}

	public DataCache getMessageCache() {
		return messageCache;
	}
	
	/*
	 * The MessageHandler advertise a new message for delivery
	 */
	public void deliverMessage(Message message) {

		confirmDeliveryToOwner(message);
		
		//BasecastNode.log( this, Level.FINE, message.getCacheId() + " deliverMessage");
		
		if(watcher != null) {
			byte[] data = messageCache.read(message.getCacheId());
			
			Message originalMessage = new Message(MessageType.ClientMessageDelivered, 
												  null, 
												  null, 
												  null, 
												  data, 
												  message.getCacheId());
			watcher.adeliver(originalMessage);
		}
		
		messageCache.free(message.getCacheId());
		messageOwnerSocket.remove(message.getCacheId());
	}
	
	
	
}
