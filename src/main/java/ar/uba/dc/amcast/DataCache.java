package ar.uba.dc.amcast;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataCache {
	
	private static final Logger LOGGER = Logger.getLogger( DataCache.class.getName() );
	
	// MessageId -> ControlMessage
    private Map<String, byte[]> cachedMessages;
	
	public DataCache() {
		cachedMessages = new ConcurrentHashMap<String, byte[]>();
	}
	
	public void store(String messageId, byte[] data) {
		
		if(data == null) {
			LOGGER.log( Level.WARNING, "Storing NULL data from id {0}", messageId);
		}else {
			LOGGER.log( Level.FINE, "Storing in cache {0} ",messageId);	
		}
		
		cachedMessages.put(messageId, data);

	}
	
	public void free(String messageId) {
		//LOGGER.log( Level.FINE, "Removing from cache: "+messageId+" "+cachedMessages.get(messageId));
		cachedMessages.remove(messageId);	
	}
	
	public byte[] read(String msgId) {
		byte[] data = cachedMessages.get(msgId);
		if(data == null) {
			LOGGER.log( Level.WARNING, "Reading NULL data from id {0}",msgId);
		}else {
			LOGGER.log( Level.FINE, "Reading from cache {0} ",msgId);	
		}
		
		return data;
	}

}
