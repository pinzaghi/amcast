package ar.uba.dc.amcast;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.api.AmcastNode;
import ar.uba.dc.amcast.api.Consensus;
import ch.usi.da.paxos.Util;
import ch.usi.da.paxos.ring.Node;
import ch.usi.da.paxos.storage.Decision;
import ch.usi.da.paxos.storage.FutureDecision;

public class URingPaxosConsensus implements Consensus {
	private Node paxosNode;
	private AmcastNode node;
	
	private String zoo_host;
	
	public URingPaxosConsensus(String zoo_host){
		this.zoo_host = zoo_host;
	}
	
	public void register(AmcastNode n) {
		this.node = n;
	}

	public FutureDecision propose(byte[] bytes) {
		return paxosNode.getProposer(paxosNode.getGroupID()).propose(bytes);
	}

	public BlockingQueue<Decision> getDecisions() {
		return paxosNode.getLearner().getDecisions();
	}

	public boolean isCoordinator(AmcastNode p) {
		return paxosNode.getRings().get(0).getRingManager().isNodeCoordinator();
	}
	
	public int getCoordinatorID(AmcastNode p) {
		return paxosNode.getRings().get(0).getRingManager().getCoordinatorID();
	}
	
	public void start() {
		try {
			this.paxosNode = new Node(node.getNodeId(),
								 node.getGroupId(),
								 zoo_host,
								 Util.parseRingsArgument(node.getGroupId()+":PAL"));
			paxosNode.start();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (KeeperException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void stop() {
		try {
			paxosNode.stop();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
