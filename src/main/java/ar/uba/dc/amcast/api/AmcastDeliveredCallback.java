package ar.uba.dc.amcast.api;

public interface AmcastDeliveredCallback {
	void messageDelivered(String messageId);
}
