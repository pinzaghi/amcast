package ar.uba.dc.amcast.api;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.List;

import ar.uba.dc.amcast.message.Message;

public interface AmcastServerCommunication extends Runnable {
	
	void send(Message m, SocketAddress to) throws IOException;
	void receive(Message m, SocketAddress from);
	String getZooKeeperHost();
	void register(AmcastNode basecastNode);
	
	void stop();
	

}
