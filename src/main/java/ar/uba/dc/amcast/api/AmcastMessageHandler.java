package ar.uba.dc.amcast.api;

import java.util.List;

import ar.uba.dc.amcast.message.Message;

public interface AmcastMessageHandler {
	
	public void handleMessage(Message deliveredMessage);
	public void handleDecision(List<Message> decided);

}
