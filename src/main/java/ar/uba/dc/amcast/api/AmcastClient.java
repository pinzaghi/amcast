package ar.uba.dc.amcast.api;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.Result;

public interface AmcastClient {
	
	public Integer getClientId();
	
	public void connectToLocalReplicas(Integer replicaId) throws KeeperException, InterruptedException, IOException;
	public void receiveFromCommunication(Message m);
	
	/**
     * Atomic multicast a message, this method blocks until its delivered
	 * @return false on timeout, true on success
	 * @throws InterruptedException 
	 * @throws IOException 
     */
	public Boolean amulticast(byte[] data, Set<Integer> dst) throws InterruptedException, IOException;
	
	/**
     * Atomic multicast a message, returns inmediatly and calls the callee on finished
	 * @return A result, succesful with a messageId or Failed if system is saturated
	 * @throws IOException 
     */
	public Result amulticast(byte[] data, Set<Integer> dst, AmcastDeliveredCallback callee) throws IOException;
	
	
	
}
