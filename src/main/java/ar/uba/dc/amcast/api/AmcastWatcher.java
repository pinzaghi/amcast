package ar.uba.dc.amcast.api;

import ar.uba.dc.amcast.message.Message;

public interface AmcastWatcher {
	
	public void adeliver(Message message);
	public void close();
	
}
