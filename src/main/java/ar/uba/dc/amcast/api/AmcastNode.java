package ar.uba.dc.amcast.api;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.List;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.DataCache;
import ar.uba.dc.amcast.message.Message;

public interface AmcastNode {

	void receiveFromCommunication(Message message, SocketAddress from);
	// Used to advertise a new message for delivery
	void deliverMessage(Message m);
	public void proposeMessages(List<Message> toOrderWithoutOrdered);
	public void handleDecision(List<Message> decided);
	
	public Integer getNodeId();
	public Integer getGroupId();
	
	public void start() throws IOException, KeeperException, InterruptedException;
	public void stop() throws IOException;
	public boolean isCoordinator();
	
	

}
