package ar.uba.dc.amcast.api;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.message.Message;

public interface AmcastClientCommunication extends Runnable {
	
	void register(AmcastClient basecastNode);

	void registerNode(Integer groupId, Integer nodeId) throws KeeperException, InterruptedException, IOException;

	Boolean sendingIsPossible(Set<Integer> dst);

	void send(Message controlMessage, Integer groupId) throws IOException;
	
}
