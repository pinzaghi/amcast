package ar.uba.dc.amcast.api;

import java.util.concurrent.BlockingQueue;

import ch.usi.da.paxos.storage.Decision;
import ch.usi.da.paxos.storage.FutureDecision;

public interface Consensus {
	
	public void register(AmcastNode p);

	public FutureDecision propose(byte[] bytes);
	public BlockingQueue<Decision> getDecisions();
	
	public void start();
	public void stop();

	boolean isCoordinator(AmcastNode p);

	int getCoordinatorID(AmcastNode node);
	
}
