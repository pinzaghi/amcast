package ar.uba.dc.amcast.message;

public enum MessageType {

	Start(0),
	SetHard(1), 
	SendHard(2), 
	SyncHard(3), 
	Final(4), 
	ClientStart(5),
	ClientMessageDelivered(6);
	
	private final int id;
	
	private MessageType(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public static MessageType fromId(int id) {
		for (MessageType t: values()){
			if (t.id == id) { return t; }
		}
		throw new RuntimeException("MessageType " + id + " does not exist!");
	}
	
	public String toString() { 
	    return this.name();
	} 
}
