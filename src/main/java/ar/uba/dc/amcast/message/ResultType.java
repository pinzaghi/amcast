package ar.uba.dc.amcast.message;

public enum ResultType {
	
	Success(0),
	TooMuchDataOnFlight(1),
	SendingNotPossible(2);
	
	private final int id;
	
	private ResultType(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public String toString() { 
	    return this.name();
	} 
}
