package ar.uba.dc.amcast.message;

public class Result {

	private ResultType type;
	private String messageId;

    public Result(ResultType type, String messageId) {
    	this.messageId = messageId;
    	this.type = type;
    }
    
    public Result(ResultType type) {
    	this.messageId = null;
    	this.type = type;
	}
    
    public String getMessageId() {
    	return this.messageId;
	}
    
    public boolean isSuccessful() {
    	return type == ResultType.Success;
	}
    
}
