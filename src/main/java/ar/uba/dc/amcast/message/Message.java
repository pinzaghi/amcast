package ar.uba.dc.amcast.message;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Message{

	private MessageType type;
	private byte[] value;
    private Set<Integer> dst;
    private Integer originGroup;
    private Long timestamp;
    private String msg_id;  
    
    public Message(MessageType type, Set<Integer> dst, Integer originGroup, Long timestamp, byte[] value, String msg_id) {
    	this.value = value;
    	this.dst = dst;
    	this.type = type;
    	this.originGroup = originGroup;
    	this.timestamp = timestamp;
    	this.msg_id = msg_id;
    }
    
    public Message(Message m) {
		this( m.getType(), m.getDestinations(), m.getOriginGroup(), m.getTimestamp(), m.getValue(), m.getCacheId() );
	}
    
    public Set<Integer> getDestinations() {
		return dst;
	}
    
    public void setCacheId(String msgId) {
    	this.msg_id = msgId;
    }
    
    public String getCacheId() {
    	return this.msg_id;
    }

    public MessageType getType() {
    	return this.type;
    }
    
    public byte[] getValue() {
    	return this.value;
    }
    
    public Integer getOriginGroup() {
    	return this.originGroup;
    }
    
    public Long getTimestamp() {
    	return this.timestamp;
    }
    
    public void setType(MessageType newType) {
    	this.type = newType;
    }
	
	public boolean equals(Object obj) {
		if(obj instanceof Message){
			Message m = (Message)obj;
            if( this.type.equals(m.type) &&
                (this.dst != null ? this.dst.equals(m.dst) : true) &&
            	(this.msg_id != null ? this.msg_id.equals(m.msg_id) : true) &&
            	(this.value != null ? Arrays.equals(this.value,m.value) : true) &&
            	(this.value == null ? this.value == m.value : true) &&
            	(this.timestamp != null ? this.timestamp.equals(m.timestamp) : true) &&
            	(this.originGroup != null ? this.originGroup.equals(m.originGroup) : true)
              ){
                    return true;
            }
		}
		return false;
	}
	
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            // if deriving: appendSuper(super.hashCode()).
        	append(msg_id).
            append(type).
            append(dst).
            append(value).
            append(timestamp).
            append(originGroup).
            toHashCode();
    }
    
	public String toString() { 	
		String id = "null";
		if(msg_id != null) {
			id = new String(msg_id);
		}
		/*
		String val = "null";
		if(value != null) {
			try {
				val = new String(value, "UTF-8");
			} catch (UnsupportedEncodingException e) {
			}
		}
		
		return "["+ type +", dst: "+dst+", origGroup: "+originGroup+", id:"+id+", ts: "+timestamp+", val: "+val+"]";
	    */
	    return "["+ type +", dst: "+dst+", origGroup: "+originGroup+", id:"+id+", ts: "+timestamp+"]";
	} 
	
	public Boolean occurredEarlierThan(Message otherMessage) {
		
		// string.compareTo if negative = is less than
		Boolean occurredEarlier = (getTimestamp() < otherMessage.getTimestamp()) || 
				(getTimestamp().equals(otherMessage.getTimestamp()) 
				&& (getCacheId().compareTo(otherMessage.getCacheId()) < 0));
		
		
		return occurredEarlier;
	}

	public static byte[] toWire(Message m){
		ByteBuffer buffer = ByteBuffer.allocate(length(m));
		toBuffer(buffer,m);
		return buffer.array();
	}
	
	public static Message fromWire(byte[] b) {
		ByteBuffer buffer = ByteBuffer.wrap(b);
		try {
			return fromBuffer(buffer);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static void toBuffer(ByteBuffer b, Message m){
		
		// int type;
		// int dst.size
	    // int[] dst;
	    // int originGroup;
	    // long timestamp;
		// int msg_id.size
	    // char[] msg_id;
		// int value.size
		// byte[] value;
		
		b.putInt(m.getType().getId());
		
		if(m.getDestinations() != null) {
			Iterator<Integer> it = m.getDestinations().iterator();
			b.putInt( m.getDestinations().size() );
			while(it.hasNext()) {
				b.putInt( it.next().intValue() );
			}
		}else {
			b.putInt(-1);
		}
		
		if(m.getOriginGroup() != null) {
			b.putInt( m.getOriginGroup() );
		}else {
			b.putInt(-1);
		}
		if(m.getTimestamp() != null) {
			b.putLong( m.getTimestamp() );
		}else {
			b.putLong(-1L);
		}
		if(m.getCacheId() != null) {
			b.putInt( m.getCacheId().getBytes().length );
			b.put(m.getCacheId().getBytes());
		}else {
			b.putInt(0);
		}
		
		if(m.getValue() != null) {
			b.putInt( m.getValue().length );
			b.put(m.getValue());
		}else {
			b.putInt(0);
		}

				
	}

	public static Message fromBuffer(ByteBuffer buffer) throws Exception {
		int type = buffer.getInt();
		MessageType mType = MessageType.fromId(type);
		
		int dstSize = buffer.getInt();
		LinkedHashSet<Integer> dsts = null;
		if(dstSize > 0) {
			dsts = new LinkedHashSet<Integer>();
			for(int i = 0 ; i < dstSize ; i++) {
				int dst = buffer.getInt();
				dsts.add(dst);
			}
		}

		int orig_group = buffer.getInt();
		Integer originGroup = orig_group;
		if(orig_group == -1) {
			originGroup = null;
		}
		
		long ts = buffer.getLong();
		Long timestamp = ts;
		if(ts == -1) {
			timestamp = null;
		}
		
		String cacheIdString = null;
		int cacheIdSize = buffer.getInt();
		if(cacheIdSize > 0) {
			byte[] cacheId = new byte[cacheIdSize];
			buffer.get(cacheId);
			cacheIdString = new String(cacheId);
		}
		
		int value_size = buffer.getInt();
		byte[] value = null;
		if(value_size > 0) {
			value = new byte[value_size];
			buffer.get(value);
		}

		Message msg = new Message(mType, dsts, originGroup, timestamp, value, cacheIdString);
		return msg;
	}
	
	/**
	 * Get the Message length (without length prefix)
	 * 
	 * @param m
	 * @return length of the message on the wire (without length prefix)
	 */
	public static int length(Message m){	
		int length = 0;
		length = length + 4; 										// int type;
		length = length + 4; 										// int dst.size
		if(m.getDestinations() != null) {
			length = length + 4*m.getDestinations().size(); 			// int[] dst;
		}
		length = length + 4; 										// int originGroup;
		length = length + 8; 										// long timestamp;
		length = length + 4; 										// int msg_id.size
		
		if(m.getCacheId() != null) {
			length = length + m.getCacheId().getBytes().length;			// char[] msg_id;	
		}
		
		length = length + 4; 										// int value.size
		if(m.getValue() != null) {
			length = length + m.getValue().length;					// byte[] value;
		}
		
		return length;
	}
	
	public static int length(List<Message> messages){	
		int length = 0;
		length = length + 4; // int: list size
		
		for(Message m : messages) {
			length = length + 4; // int: message size
			length = length + length(m);
		}
		
		return length;
	}
	
	public static byte[] listToWire(List<Message> messages){
		ByteBuffer buffer = ByteBuffer.allocate(length(messages));
		listToBuffer(buffer,messages);
		return buffer.array();
	}
	
	public static List<Message> listFromWire(byte[] b) {
		ByteBuffer buffer = ByteBuffer.wrap(b);
		try {
			return listFromBuffer(buffer);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static void listToBuffer(ByteBuffer b, List<Message> messages){
		
		b.putInt(messages.size());
		for(Message m : messages) {
			b.putInt(length(m));
			toBuffer(b,m);
		}
				
	}

	public static List<Message> listFromBuffer(ByteBuffer buffer) throws Exception {
		int size = buffer.getInt();

		LinkedList<Message> messages = new LinkedList<Message>();
		
		if(size > 0) {
			for(int i = 0; i < size; i++) {
				int message_size = buffer.getInt();
				byte[] message_bytes = new byte[message_size];
				buffer.get(message_bytes);
				Message m = fromWire(message_bytes);
				messages.add(m);
			}
			
		}

		return messages;
	}
	
	public static int listLength(List<List<Message>> messages){	
		int length = 0;
		length = length + 4; // int: list size
		
		for(List<Message> m : messages) {
			length = length + 4;
			length = length + length(m);
		}
		
		return length;
	}
	
	public static byte[] listOfListsToWire(List<List<Message>> messages){
		ByteBuffer buffer = ByteBuffer.allocate(listLength(messages));
		listOfListToBuffer(buffer,messages);
		return buffer.array();
	}
	
	public static void listOfListToBuffer(ByteBuffer b, List<List<Message>> messages){
		
		b.putInt(messages.size());
		for(List<Message> m : messages) {
			b.putInt(length(m));
			listToBuffer(b,m);
		}
				
	}
	
	public static List<List<Message>> listOfListsFromWire(byte[] b) {
		ByteBuffer buffer = ByteBuffer.wrap(b);
		try {
			return listOfListsFromBuffer(buffer);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static List<List<Message>> listOfListsFromBuffer(ByteBuffer buffer) throws Exception {
		int size = buffer.getInt();

		List<List<Message>> messages = new LinkedList<List<Message>>();
		
		if(size > 0) {
			for(int i = 0; i < size; i++) {
				int list_size = buffer.getInt();
				byte[] message_bytes = new byte[list_size];
				buffer.get(message_bytes);
				List<Message> list = listFromWire(message_bytes);
				messages.add(list);
			}
			
		}

		return messages;
	}


}
