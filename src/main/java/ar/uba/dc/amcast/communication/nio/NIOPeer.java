package ar.uba.dc.amcast.communication.nio;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.message.Message;

@Deprecated
public abstract class NIOPeer {
	
	private Boolean running = true;
	
	private static final Logger LOGGER = Logger.getLogger( NIOPeer.class.getName() );
	
	// A list of PendingChange instances
	protected List<ChangeRequest> pendingChanges = new LinkedList<ChangeRequest>();
	
	// The buffer into which we'll read data when it's available
	protected ByteBuffer readBuffer = ByteBuffer.allocate(8192);
	
	// Maps a SocketChannel to a list of ByteBuffer instances
	protected Map<SocketChannel, List<ByteBuffer>> pendingData = new HashMap<SocketChannel, List<ByteBuffer>>();
	
	// The selector we'll be monitoring
	protected Selector selector;
	
	protected LinkedHashMap<SocketAddress,SocketChannel> connections = new LinkedHashMap<SocketAddress,SocketChannel>();
		
	public void run() {
		
		try {
			this.selector = initSelector();
		} catch (IOException | KeeperException | InterruptedException e) {
			StringWriter sw = new StringWriter();
	    	PrintWriter pw = new PrintWriter(sw);
	    	e.printStackTrace(pw);
	    	
	    	LOGGER.log(Level.SEVERE, "Exception {0}", sw);
		}
		
		while (running) {
			try {
				// Process any pending changes
				synchronized (this.pendingChanges) {

					Iterator<ChangeRequest> changes = this.pendingChanges.iterator();
					
					while (changes.hasNext()) {
							ChangeRequest change = (ChangeRequest) changes.next();
							
							LOGGER.log( Level.FINE, "Processing ChangeRequest {0}", change);
							
							switch (change.type) {
								case ChangeRequest.CHANGEOPS:
									SelectionKey key = change.socket.keyFor(this.selector);
									key.interestOps(change.ops);
									break;
								case ChangeRequest.REGISTER:
									change.socket.register(this.selector, change.ops);
									break;
							}
					}
					this.pendingChanges.clear();
				}

				// Wait for an event one of the registered channels
				this.selector.select();

				// Iterate over the set of keys for which events are available
				Iterator selectedKeys = this.selector.selectedKeys().iterator();
				while (selectedKeys.hasNext()) {
					SelectionKey key = (SelectionKey) selectedKeys.next();
					selectedKeys.remove();

					if (!key.isValid()) {
						continue;
					}
					
					// Check what event is available and deal with it
					if (key.isConnectable()) {
						this.finishConnection(key);
					}else if (key.isAcceptable()) {
						this.accept(key);
					} else if (key.isReadable()) {
						this.read(key);
					} else if (key.isWritable()) {
						this.write(key);
					}
					
				}
			} catch (Exception e) {
				StringWriter sw = new StringWriter();
		    	PrintWriter pw = new PrintWriter(sw);
		    	e.printStackTrace(pw);

				LOGGER.log( Level.SEVERE, "Exception {0}",sw);
				
				stop();
			}
		}
	}
	
	protected void read(SelectionKey key) throws IOException {

		SocketChannel socketChannel = (SocketChannel) key.channel();

		// Clear out our read buffer so it's ready for new data
		this.readBuffer.clear();

		// Attempt to read off the channel
		int numRead;
		try {
			numRead = socketChannel.read(this.readBuffer);
			
			LOGGER.log( Level.FINE, "READ: Reading buffer for channel, size {0}", numRead);
			
		} catch (IOException e) {
			// The remote forcibly closed the connection, cancel
			// the selection key and close the channel.
			LOGGER.log( Level.SEVERE, "ERROR {0}",e);
			key.cancel();
			socketChannel.close();
			return;
		}

		if (numRead == -1) {
			// Remote entity shut the socket down cleanly. Do the
			// same from our end and cancel the channel.
			LOGGER.log( Level.FINE, "Remote entity shut the socket down cleanly");
			key.channel().close();
			key.cancel();
			
			return;
		}

		// Handle the response
		this.handleResponse(socketChannel, this.readBuffer.array(), numRead);
	}
	
	protected void write(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();
		
		LOGGER.log( Level.FINE, "Channel is writeable {0}",socketChannel.getRemoteAddress());

		synchronized (this.pendingData) {
			
			LOGGER.log( Level.FINE, "WRITE: Writing buffer for channel {0}",key.channel());
			
			List<ByteBuffer> queue = (List<ByteBuffer>) this.pendingData.get(socketChannel);

			// Write until there's not more data ...
			while (!queue.isEmpty()) {
				ByteBuffer buf = (ByteBuffer) queue.get(0);
				socketChannel.write(buf);
				if (buf.remaining() > 0) {
					// ... or the socket's buffer fills up
					break;
				}
				queue.remove(0);
			}

			if (queue.isEmpty()) {
				// We wrote away all data, so we're no longer interested
				// in writing on this socket. Switch back to waiting for
				// data.
				key.interestOps(SelectionKey.OP_READ);
				
				LOGGER.log( Level.FINE, "We wrote away all data, so we're no longer interested in writing on this socket");
			}
		}
	}
	
	public void connect(SocketAddress addr) throws IOException {
		
		LOGGER.log( Level.FINE, "Init connection");
		
		// Create a non-blocking socket channel
		SocketChannel socketChannel = SocketChannel.open();
		socketChannel.configureBlocking(false);
		socketChannel.setOption(StandardSocketOptions.TCP_NODELAY, true);
	
		// Kick off connection establishment
		socketChannel.connect(addr);
		
		// Save connection for later use
		connections.put(addr, socketChannel);
	
		// Queue a channel registration since the caller is not the 
		// selecting thread. As part of the registration we'll register
		// an interest in connection events. These are raised when a channel
		// is ready to complete connection establishment.
		synchronized(this.pendingChanges) {
			this.pendingChanges.add(new ChangeRequest(socketChannel, ChangeRequest.REGISTER, SelectionKey.OP_CONNECT));
		}
		
		selector.wakeup();

	}
	
	protected void accept(SelectionKey key) {
		
		try {

			// For an accept to be pending the channel must be a server socket channel.
			ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
	
			// Accept the connection and make it non-blocking
			SocketChannel socketChannel = serverSocketChannel.accept();
			Socket socket = socketChannel.socket();
			socketChannel.configureBlocking(false);
			socketChannel.setOption(StandardSocketOptions.TCP_NODELAY, true);
			
			LOGGER.log( Level.FINE, "Accepting connection {0}",socket);
			
			connections.put(socket.getRemoteSocketAddress(), socketChannel);
	
			// Register the new SocketChannel with our Selector, indicating
			// we'd like to be notified when there's data waiting to be read
			socketChannel.register(this.selector, SelectionKey.OP_READ);
		
		} catch (final IOException e) {
			LOGGER.log( Level.SEVERE, "ERROR {0}",e);
        }
	}
	
	public void send(Message m, SocketAddress addr) throws IOException {
		
		LOGGER.log( Level.FINE, "Sending message "+m);
		
		byte[] data = Message.toWire(m);
		
		if(!connections.containsKey(addr)) {
			//LOGGER.log( Level.FINE, "New connection to "+addr+" for sending "+m);
			connect(addr);
		}
		
		SocketChannel socket = connections.get(addr);
		
		synchronized (this.pendingChanges) {
			// Indicate we want the interest ops set changed
			LOGGER.log( Level.FINE, "Schedule write op for sending {0}",m);
			this.pendingChanges.add(new ChangeRequest(socket, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));

			// And queue the data we want written
			synchronized (this.pendingData) {
				
				LOGGER.log( Level.FINE, "Queuing pending message to send {0}",m);
				
				List<ByteBuffer> queue = (List<ByteBuffer>) this.pendingData.get(socket);
				if (queue == null) {
					queue = new ArrayList<ByteBuffer>();
					this.pendingData.put(socket, queue);
				}
				queue.add(ByteBuffer.wrap(data));
			}
		}

		// Finally, wake up our selecting thread so it can make the required changes
		this.selector.wakeup();
	}
	
	abstract public void receive(Message m, SocketAddress from);
	
	protected abstract Selector initSelector() throws IOException, KeeperException, InterruptedException;

	void handleResponse(SocketChannel socketChannel, byte[] data, int numRead) throws IOException {
		LOGGER.log( Level.FINE, "Handling response from {0} bytes to read {1}", new Object[] {socketChannel.getRemoteAddress(), numRead});
		// Make a correctly sized copy of the data before handing it
		// to the client
		byte[] rspData = new byte[numRead];
		System.arraycopy(data, 0, rspData, 0, numRead);
		
		Message m = Message.fromWire(rspData);
		
		LOGGER.log(Level.FINE, "Message read: {0}", m);
		
		receive(m, socketChannel.getRemoteAddress());
		
	}

	protected void finishConnection(SelectionKey key) throws IOException {
		SocketChannel socketChannel = (SocketChannel) key.channel();
		
		LOGGER.log( Level.FINE, "Finish connection to {0}",socketChannel.getRemoteAddress());
	
		// Finish the connection. If the connection operation failed
		// this will raise an IOException.
		try {
			socketChannel.finishConnect();
		} catch (IOException e) {
			LOGGER.log( Level.SEVERE, "ERROR {0}", e);
			key.cancel();
			return;
		}
	
		// Register an interest in writing on this channel
		key.interestOps(SelectionKey.OP_READ);
	}
	
	public void stop() {
		running = false;
	}

}
