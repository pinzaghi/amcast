package ar.uba.dc.amcast.communication.io;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.api.AmcastNode;
import ar.uba.dc.amcast.api.AmcastServerCommunication;
import ar.uba.dc.amcast.communication.ZookeeperUtils;
import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.MessageType;

public class ServerCommunication extends Peer implements AmcastServerCommunication {
    
    private static final Logger LOGGER = Logger.getLogger( ServerCommunication.class.getName() );
    
    // MessageId -> SocketAddress

    private AmcastNode node;
	private InetSocketAddress socketAddress;
	private String zoo_host;
	
	// For server mode
	protected int          serverPort   = 0;
	protected ServerSocket serverSocket = null;

    public ServerCommunication(String zoo_host) throws KeeperException, InterruptedException{
    	super();
    	this.zoo_host = zoo_host;
    }
    
    public void register(AmcastNode node) {
    	this.node = node;
    }
    
    // Entry point for connection
    public void receive(Message m, SocketAddress from) {
    	node.receiveFromCommunication(m, from);
    }
    
    public InetSocketAddress getSocketAddress() {
		return this.socketAddress;
	}

    public void run(){
    	
    	InetSocketAddress serverAddress = null;
		try {
			serverAddress = ZookeeperUtils.getNodeSocketAddress(zoo_host, node.getGroupId(), node.getNodeId());
		} catch (KeeperException | InterruptedException | IOException e) {
			StringWriter sw = new StringWriter();
	    	PrintWriter pw = new PrintWriter(sw);
	    	e.printStackTrace(pw);
	    	
	    	LOGGER.log(Level.SEVERE, "Exception {0} {1}", new Object[] {e, sw.toString()});
		}
    	
    	if(node == null) {
    		throw new RuntimeException("Local endpoint is not connected");
    	}

        startServerLoop(serverAddress);
    }
    
    public void startServerLoop(InetSocketAddress listenAddr) {
        
        serverPort = listenAddr.getPort();
        
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
        } catch (IOException e) {
        	this.running = false;
            throw new RuntimeException("Cannot open port "+this.serverPort, e);
        }
        
        synchronized(this){
            runningThread = Thread.currentThread();
            running = true;
        }
        
        while(isRunning()){
            Socket clientSocket = null;
            try {
            	clientSocket = this.serverSocket.accept();
            	clientSocket.setTcpNoDelay(tcp_nodelay);
            	clientSocket.setReceiveBufferSize(buffer_size);
            	clientSocket.setSendBufferSize(buffer_size);

                SocketAddress socketAddress = clientSocket.getRemoteSocketAddress();

                LOGGER.log(Level.FINE, "New client connection {0}", new Object[] {socketAddress});
                
                connect(socketAddress, clientSocket);
                
            } catch (IOException e) {
                if(!isRunning()) {
                    return;
                }
                throw new RuntimeException("Error accepting client connection", e);
            } 
            
        }

	}
    
    public static void log(ServerCommunication p, Level level, String log) {
		LOGGER.log(level, p.node+" ServerCommunication: "+log+", timestamp "+System.nanoTime());	
	}

	public String getZooKeeperHost() {
		return zoo_host;
	}
	
	public synchronized void stop(){
        this.running = false;
        
        for(ConnectionHandler connection : establishedConnections.values()) {
			connection.close();
		}
        
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

	

}
