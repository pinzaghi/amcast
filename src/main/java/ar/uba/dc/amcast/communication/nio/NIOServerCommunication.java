package ar.uba.dc.amcast.communication.nio;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.api.AmcastNode;
import ar.uba.dc.amcast.api.AmcastServerCommunication;
import ar.uba.dc.amcast.communication.ZookeeperUtils;
import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.MessageType;

@Deprecated
public class NIOServerCommunication extends NIOPeer implements AmcastServerCommunication {
    
    private static final Logger LOGGER = Logger.getLogger( NIOServerCommunication.class.getName() );

    protected String zoo_host;
    
    // MessageId -> SocketAddress
    private Map<String, SocketAddress> messageOwnerSocket;
    
    private ServerSocketChannel serverChannel;
    
    private AmcastNode node;

    public NIOServerCommunication(String zoo_host){
    	super();
    	this.zoo_host = zoo_host;
    	this.messageOwnerSocket = new ConcurrentHashMap<String, SocketAddress>();
    }
    
    public void register(AmcastNode node) {
    	this.node = node;
    }
    
    protected Selector initSelector() throws IOException, KeeperException, InterruptedException  {
		// Create a new selector
		Selector socketSelector = SelectorProvider.provider().openSelector();

		// Create a new non-blocking server socket channel
		this.serverChannel = ServerSocketChannel.open();
		serverChannel.configureBlocking(false);
		
		InetSocketAddress isa = ZookeeperUtils.getNodeSocketAddress(zoo_host, node.getGroupId(), node.getNodeId());

		// Bind the server socket to the specified address and port
		serverChannel.socket().bind(isa);

		// Register the server socket channel, indicating an interest in 
		// accepting new connections
		serverChannel.register(socketSelector, SelectionKey.OP_ACCEPT);
		
		NIOServerCommunication.log(this, Level.FINE, "listening connections in "+isa);

		return socketSelector;
	}

    public Boolean messageOwnerKnown(Message m) {
		return messageOwnerSocket.containsKey(m.getCacheId());
	}
	
	public SocketAddress getOwnerSocketAddress(Message m) {
		return messageOwnerSocket.get(m.getCacheId());
	}

    
    public List<InetSocketAddress> getMembersAddress(int groupId){
    	
    	List<InetSocketAddress> addrs = new ArrayList<InetSocketAddress>();
    	
    	try {

    		List<String> nodes = ZookeeperUtils.getNodesFromGroup(zoo_host, groupId);
    		
    		if(nodes.size() > 0) {
    			for(String nodeId : nodes) {
    				
    				// We don't want to send messages to ourselves
    				if(groupId != node.getGroupId() || (groupId == node.getGroupId() && Integer.valueOf(nodeId) != node.getNodeId())) {
		    	    
			    	    InetSocketAddress commChannelSockAddr = ZookeeperUtils.getNodeSocketAddress(zoo_host, Integer.valueOf(groupId), Integer.valueOf(nodeId));
			    	    
			    	    addrs.add(commChannelSockAddr);
		    	    
    				}
    			}
    		}else {
    			NIOServerCommunication.log(this, Level.WARNING, "Group #"+groupId+" has no processes running." );
    		}
    	} catch (Exception e) {
			NIOServerCommunication.log(this, Level.SEVERE, "Exception ocurred on getMembersAddress "+e);
		}
    	
    	return addrs;
    	
    }    
	
    public static void log(NIOServerCommunication p, Level level, String log) {
		LOGGER.log(level, p.node+" NetworkCommunication: "+log);	
	}

	public void receive(Message m, SocketAddress from) {
		NIOServerCommunication.log(this, Level.FINE, "Received message "+m+" from "+from);
    	
    	node.receiveFromCommunication(m, from);
	}
	
	public String getZooKeeperHost() {
		return zoo_host;
	}

}
