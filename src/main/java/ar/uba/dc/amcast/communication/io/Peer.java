package ar.uba.dc.amcast.communication.io;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import ar.uba.dc.amcast.message.Message;

public abstract class Peer {
	
	private static final Logger LOGGER = Logger.getLogger( Peer.class.getName() );
	
	protected Map<SocketAddress, ConnectionHandler> establishedConnections;
	protected Map<SocketAddress, Socket> establishedConnectionsSockets;

    protected boolean      running    	= false;
    protected Thread       runningThread= null;
    
    public boolean tcp_nodelay = true;
	public int buffer_size = 32*1024;
	
	public Peer() {  
		this.establishedConnections = new ConcurrentHashMap<SocketAddress, ConnectionHandler>();
		this.establishedConnectionsSockets = new ConcurrentHashMap<SocketAddress, Socket>();
	}
	
	public void connect(SocketAddress socketAddr, Socket socket) throws IOException {
		
		//Peer.log( this, Level.FINER, "Register connection to node "+socketAddr);
		
		if(socket == null) {
			socket = new Socket();
			socket.setTcpNoDelay(tcp_nodelay);
			socket.setReceiveBufferSize(buffer_size);
			socket.setSendBufferSize(buffer_size);
			socket.connect(socketAddr);
		}
		
		registerConnection(socketAddr, socket);

		Thread t = new Thread(establishedConnections.get(socketAddr));
		t.setName("ConnectionHandler "+socket.getRemoteSocketAddress());
		t.start();
		
		LOGGER.log(Level.FINER, "ConnectionHandler for socket launched from {0} to {1}", new Object[] {socket.getLocalAddress(),socket.getRemoteSocketAddress()});
	}
	
	abstract public void receive(Message m, SocketAddress from);
	
	public void send(Message message, SocketAddress to) {
		
		LOGGER.log(Level.FINE, "Peer.send {0} to {1}", new Object[] {message, to});
		
		try {	

			if(!establishedConnections.containsKey(to)){
				LOGGER.log(Level.FINE, "Establishing new connection to {1} for sending {0}", new Object[] {message, to});
				connect(to, null);
			}

			establishedConnections.get(to).send(message);
	        
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
	    	PrintWriter pw = new PrintWriter(sw);
	    	e.printStackTrace(pw);
	    	
	    	LOGGER.log(Level.SEVERE, "Exception {0} {1} sending {2} to {3}", new Object[] {e, sw.toString(),message, to});
		}

	}
	
	public void registerConnection(SocketAddress addr, Socket socket) throws IOException {
		ConnectionHandler connectionHandler = new ConnectionHandler(this, socket);
		establishedConnections.put(addr, connectionHandler);
		establishedConnectionsSockets.put(addr, socket);
    }

	public synchronized boolean isRunning() {
        return this.running;
    }
	
	public static void log(Peer p, Level level, String log) {
		LOGGER.log( level, p.toString()+" "+log+ ",timestamp "+System.nanoTime());	
	}

}
