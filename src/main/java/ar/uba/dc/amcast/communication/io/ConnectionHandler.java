package ar.uba.dc.amcast.communication.io;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import ar.uba.dc.amcast.message.Message;

public class ConnectionHandler implements Runnable{
	
	private static final Logger LOGGER = Logger.getLogger( ConnectionHandler.class.getName() );
	
	InputStream is;
	OutputStream os;
	Peer client;
	Socket socket;
	BlockingQueue<Message> sendingQueue;
	BlockingQueue<Message> receivedQueue;
	
	boolean running;
	
	public ConnectionHandler(Peer launcherClient, Socket socket) throws IOException {
		this.is = socket.getInputStream();
		this.os = socket.getOutputStream();
		this.client = launcherClient;
		this.socket = socket;
		this.sendingQueue = new LinkedBlockingQueue<Message>();
		this.receivedQueue = new LinkedBlockingQueue<Message>();
		
	}

	public void run() {
		
		synchronized(this) {
			this.running = true;
		}
		//ConnectionHandler.log( Level.FINER, client, " launched.");
		
		Thread reader = new Thread() { public void run() { readerLoop(); } };
		reader.setName(socket.getRemoteSocketAddress()+" Reader");
		
		Thread writer = new Thread() { public void run() { writerLoop(); } };
		writer.setName(socket.getRemoteSocketAddress()+" Writer");

		reader.start();
		writer.start();
		
		while(true) {
			Message m = null;
			try {
				m = receivedQueue.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			client.receive(m, socket.getRemoteSocketAddress());
		}

	}
	
	private void readerLoop() {
		Message m;

		//ConnectionHandler.log( Level.FINER, client," Waiting for new message... ");
		
		DataInputStream in = new DataInputStream(is);
		byte[] receiveBytes;
		
		while(isRunning()) {
			
			try {

				//final byte[] receiveSizeBytes = new byte[4];
				//in.readFully(receiveSizeBytes);
				//final int receiveSize = ByteBuffer.wrap(receiveSizeBytes).getInt();
				int receiveSize = in.readInt();

                receiveBytes = new byte[receiveSize];			

                in.readFully(receiveBytes);

                m = Message.fromWire(receiveBytes);
                receiveBytes = null;
                
		        if(m != null) {
		        	receivedQueue.put(m);
		        	LOGGER.log( Level.FINE," received a {0} through connectionHandler.read", m);
		            
		        }else {
		        	LOGGER.log( Level.SEVERE, "Message received as NULL.");
		        }
		        
		        LOGGER.log( Level.FINER, "Waiting for new message... ");
	        
			}catch(EOFException e) {
		    	break;
			} catch (Exception e) {
		    	StringWriter sw = new StringWriter();
		    	PrintWriter pw = new PrintWriter(sw);
		    	e.printStackTrace(pw);
		    	
		    	LOGGER.log( Level.FINE, "Exception {0}", sw.toString());
		    	
		    	close();
			}

		}
	}
	
	public void send(Message message) throws InterruptedException {
		sendingQueue.put(message);
	}
	
	private void writerLoop() {
		
		while(isRunning()) {
			try {
				
				Message message = sendingQueue.take();

				final byte[] sendBytes = Message.toWire(message);
	            os.write(ByteBuffer.allocate(4).putInt(sendBytes.length).array());
	            os.write(sendBytes);
	            os.flush();
				
				//ConnectionHandler.log(Level.FINE, client, "Sending "+message+" took "+difference+" through "+socket);
				
		        
			} catch (Exception e) {
				StringWriter sw = new StringWriter();
		    	PrintWriter pw = new PrintWriter(sw);
		    	e.printStackTrace(pw);
		    	
		    	LOGGER.log( Level.FINE, "Exception {0}", sw.toString());
		    	
		    	close();
			}
			
		}
		
	}
	
	private synchronized boolean isRunning() {
		return running;
	}

	public Socket getSocket() {
		return socket;
	}
	
	public synchronized void close() {
		running = false;
		try {
			socket.close();
		} catch (IOException e) {
			//ConnectionHandler.log( Level.FINE, client, ": Exception "+e);
		}
	}
	
	public static void log(Level level, Peer c, String log) {
		LOGGER.log(level, c.toString() + " " + Thread.currentThread().getName()+" "+log+", timestamp"+System.nanoTime());	
	}

}
