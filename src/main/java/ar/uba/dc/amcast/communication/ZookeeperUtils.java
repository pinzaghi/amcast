package ar.uba.dc.amcast.communication;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs.Ids;

import ar.uba.dc.amcast.api.AmcastNode;
import ar.uba.dc.amcast.basecast.MessageHandler;

import org.apache.zookeeper.ZooKeeper;

public class ZookeeperUtils {
	
	private static final Logger LOGGER = Logger.getLogger( ZookeeperUtils.class.getName() );
	
	public static void configZookeeper(Integer groupID, String zoo_host) throws KeeperException, InterruptedException, IOException {
		
		LOGGER.log(Level.FINE, "Configuring ZooKeeper");
		
		ZooKeeperConnection conn;
		ZooKeeper zoo;
		
		conn = new ZooKeeperConnection();
		zoo = conn.connect(zoo_host);
		
		if(zoo.exists("/ringpaxos",false) == null){
			zoo.create("/ringpaxos", null, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		}
		
		if(zoo.exists("/ringpaxos/topology"+groupID,false) == null){
			zoo.create("/ringpaxos/topology"+groupID, null, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		}
		
		if(zoo.exists("/ringpaxos/topology"+groupID+"/config",false) == null){
			zoo.create("/ringpaxos/topology"+groupID+"/config", null, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		}
		
		if(zoo.exists("/ringpaxos/topology"+groupID+"/config/stable_storage",false) == null){
			zoo.create("/ringpaxos/topology"+groupID+"/config/stable_storage", null, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		}
		
		if(zoo.exists("/ringpaxos/topology"+groupID+"/config/concurrent_values",false) == null){
			zoo.create("/ringpaxos/topology"+groupID+"/config/concurrent_values", null, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		}
		
		String storage = "ch.usi.da.paxos.storage.InMemory";
		//String storage = "ch.usi.da.paxos.storage.BufferArray";
		
		zoo.setData("/ringpaxos/topology"+groupID+"/config/stable_storage",storage.getBytes(),-1);
		zoo.setData("/ringpaxos/topology"+groupID+"/config/concurrent_values","128".getBytes(),-1);
		zoo.close();
	}
	
	public static InetSocketAddress getNodeSocketAddress(String zoo_host, Integer groupId, Integer nodeId) throws KeeperException, InterruptedException, IOException {
		
		LOGGER.log(Level.FINE, "ZooKeeper getNodeSocketAddress");
		
		InetSocketAddress socketAddress = null;
		ZooKeeperConnection conn;
		ZooKeeper zoo;
		
		conn = new ZooKeeperConnection();
		zoo = conn.connect(zoo_host);
	
		byte[] b = zoo.getData("/ringpaxos/topology" + String.valueOf(groupId) + "/nodes/" + String.valueOf(nodeId), false, null);
	    String s = new String(b);
	    
		String ip = s.split(";")[0];
	    String port = s.split(";")[1];
	    
	    // TODO: +1 Convention
	    socketAddress = new InetSocketAddress(ip, Integer.parseInt(port)+1);
	    
	    zoo.close();

	    return socketAddress;
		
	}
	
	public static List<String> getNodesFromGroup(String zoo_host, Integer groupId) throws KeeperException, InterruptedException, IOException{
		
		LOGGER.log(Level.FINE, "ZooKeeper getNodesFromGroup");
		
		ZooKeeperConnection conn;
		ZooKeeper zoo;
		
		conn = new ZooKeeperConnection();
		zoo = conn.connect(zoo_host);
		
		List<String> nodes = zoo.getChildren("/ringpaxos/topology" + groupId + "/nodes", false);
		zoo.close();
		
		return nodes;
	}
	
	public static List<InetSocketAddress> getMembersAddress(int groupId, AmcastNode callerNode, String zoo_host) throws KeeperException, InterruptedException, IOException{
    	
		List<String> nodes = ZookeeperUtils.getNodesFromGroup(zoo_host, groupId);
		List<InetSocketAddress> addrs = new ArrayList<InetSocketAddress>();
		
		if(nodes.size() > 0) {
			for(String nodeId : nodes) {
				
				// We don't want to send messages to ourselves
				if(groupId != callerNode.getGroupId() || (groupId == callerNode.getGroupId() && Integer.valueOf(nodeId) != callerNode.getNodeId())) {
	    	    
		    	    InetSocketAddress commChannelSockAddr = ZookeeperUtils.getNodeSocketAddress(zoo_host, Integer.valueOf(groupId), Integer.valueOf(nodeId));
		    	    
		    	    addrs.add(commChannelSockAddr);
	    	    
				}
			}
		}
		
		return addrs;
    	
    }

}
