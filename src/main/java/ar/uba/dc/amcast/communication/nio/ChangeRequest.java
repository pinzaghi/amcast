package ar.uba.dc.amcast.communication.nio;

import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public class ChangeRequest {
	// types
	public static final int REGISTER = 1;
	public static final int CHANGEOPS = 2;
	
	public SocketChannel socket;
	public int type;
	public int ops;
	
	public ChangeRequest(SocketChannel socket, int type, int ops) {
		this.socket = socket;
		this.type = type;
		this.ops = ops;
	}
	
	public String toString() {
		
		String waitingFor;
		String typeStr;
		
		switch(type) {
		  case REGISTER :
			  typeStr = "register"; break;
		    
		  case CHANGEOPS :
			  typeStr = "CHANGEOPS"; break;      
		    
		  default :
			  typeStr = "" + type;  
		}
		
		switch(ops) {
		  case SelectionKey.OP_READ :
		    waitingFor = "read"; break;
		    
		  case SelectionKey.OP_WRITE :
		    waitingFor = "write"; break;      
		    
		  case SelectionKey.OP_CONNECT :
		    waitingFor = "connect"; break;
		    
		  default :
		    waitingFor = "" + ops;  
		}

		return "["+socket+", "+typeStr+", "+waitingFor+"]";
	}
}