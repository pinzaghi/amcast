package ar.uba.dc.amcast.communication.io;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.Selector;
import java.nio.channels.spi.SelectorProvider;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.api.AmcastClient;
import ar.uba.dc.amcast.api.AmcastClientCommunication;
import ar.uba.dc.amcast.communication.ZookeeperUtils;
import ar.uba.dc.amcast.message.Message;

public class ClientCommunication extends Peer implements AmcastClientCommunication {
	
	protected String zoo_host;
	protected AmcastClient client;
	private LinkedHashMap<Integer, InetSocketAddress> designatedReplicaPerGroup;
	
	public ClientCommunication(String zoo_host) throws KeeperException{
    	super();
    	this.zoo_host = zoo_host;
    	// Map for a groupId -> preferred node socket address of all regions
    	this.designatedReplicaPerGroup = new LinkedHashMap<Integer, InetSocketAddress>();
    }
	
	public void register(AmcastClient client) {
    	this.client = client;
    }
	
	protected Selector initSelector() throws IOException {
		return SelectorProvider.provider().openSelector();
	}
	
	public void receive(Message m, SocketAddress from) {
		client.receiveFromCommunication(m);
	}
	
	public Boolean sendingIsPossible(Set<Integer> dst) {
		Boolean sendingIsPossible = false;
		for(Integer groupId : dst) {
			if(designatedReplicaPerGroup.containsKey(groupId)) {
				sendingIsPossible = true;
			}
		}
		return sendingIsPossible;
	}
	public void registerNode(Integer groupId, Integer nodeId) throws KeeperException, InterruptedException, IOException {
		
		InetSocketAddress nodeSocketAddr = ZookeeperUtils.getNodeSocketAddress(zoo_host,groupId,nodeId);
		
		designatedReplicaPerGroup.put(groupId, nodeSocketAddr);
		
		connect(nodeSocketAddr, null);

	}

	@Override
	public void send(Message controlMessage, Integer groupId) throws IOException {
		InetSocketAddress nodeAddr = designatedReplicaPerGroup.get(groupId);
		send(controlMessage, nodeAddr);
	}
	
	public void run() {
		
	}


}
