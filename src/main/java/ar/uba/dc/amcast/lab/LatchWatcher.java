package ar.uba.dc.amcast.lab;

import java.util.concurrent.CountDownLatch;

import ar.uba.dc.amcast.api.AmcastWatcher;
import ar.uba.dc.amcast.message.Message;

public class LatchWatcher implements AmcastWatcher {
	
	public CountDownLatch latch;
	
	public LatchWatcher() {
		this.latch = new CountDownLatch(1);
	}
	
	public void adeliver(Message message) {
		//System.out.println("Deliver "+message);
		latch.countDown();
	}
	
	public CountDownLatch resetLatch() {
		latch = new CountDownLatch(1);
		return latch;
	}

	public void close() {
	}
}