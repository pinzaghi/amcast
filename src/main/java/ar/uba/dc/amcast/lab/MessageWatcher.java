package ar.uba.dc.amcast.lab;

import java.util.LinkedList;

import ar.uba.dc.amcast.api.AmcastWatcher;
import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.Message;

public class MessageWatcher implements AmcastWatcher {
	
	public LinkedList<Message> deliveredMessages;
	
	public MessageWatcher() {
		deliveredMessages = new LinkedList<Message>();
	}
	
	@Override
	public void adeliver(Message message) {
		deliveredMessages.add(message);
	}
	
	public String toString() {
		return deliveredMessages.toString();
	}

	public void close() {
	}

}
