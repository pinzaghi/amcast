package ar.uba.dc.amcast.lab;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;

import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.MessageType;
import ch.usi.da.paxos.Util;
import ch.usi.da.paxos.lab.DummyWatcher;
import ch.usi.da.paxos.ring.Node;
import ch.usi.da.paxos.ring.RingDescription;
import ch.usi.da.paxos.storage.Decision;
import ch.usi.da.paxos.storage.FutureDecision;

public class BenchmarkURingPaxos {

	public static void main(String[] args) throws InterruptedException, IOException, KeeperException {
		
		Integer thread_count = Integer.valueOf(args[0]);
		Integer message_count = Integer.valueOf(args[1]);
		
		Logger logger = Logger.getLogger("ch.usi.da");
		Node n1;
		Node n2;
		Node n3;

		ZooKeeper zoo = new ZooKeeper("127.0.0.1:2181",1000,new DummyWatcher());
		String path = "/ringpaxos/topology1/config/stable_storage";
		String data = "ch.usi.da.paxos.storage.InMemory";
		zoo.setData(path,data.getBytes(),-1);
		zoo.close();
		
		logger.setLevel(Level.INFO);
		n1 = new Node(1,"localhost:2181",Util.parseRingsArgument("1:PAL"));
		n2 = new Node(2,"localhost:2181",Util.parseRingsArgument("1:PAL"));
		n3 = new Node(3,"localhost:2181",Util.parseRingsArgument("1:PAL"));
		n1.start();
		n2.start();
		n3.start();
		Thread.sleep(6000); // wait until ring is fully started 
		
		long experiment_start = System.nanoTime();
		
		List<Thread> threads = new LinkedList<Thread>();
		
		Thread t;

		for(int i = 1; i <= thread_count; i++) {
	
			t = new Thread() {
				  public void run() {
					  for(int j=0;j<message_count;j++){
							//System.out.println("Proposing "+ i);
							
						  	byte[] data = ("test").getBytes();
						  	LinkedList<Message> messages = new LinkedList<Message>();
							LinkedHashSet<Integer> dst = new LinkedHashSet<Integer>();
							dst.add(1);
							Message m = new Message(MessageType.ClientStart, dst, null, null, data, UUID.randomUUID().toString());
							messages.add(m);
							
							try {
								FutureDecision fd = n1.getProposer(1).propose((""+j).getBytes());
								fd.getDecision();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
					  }
				  }
				 };
				 
			 threads.add(t);
			 t.start();
		}
		
		for(Thread tr : threads) {
			tr.join();
		}

		double experiment_seconds = (System.nanoTime() - experiment_start) / 1e9;
		
		double throughput = thread_count*message_count / experiment_seconds;
		
		System.out.println("Running time "+experiment_seconds);
		
		System.out.println("Throughput "+throughput);
		
		n1.stop();
		n2.stop();
		n3.stop();
		
		System.exit(0);
	}

	

}
