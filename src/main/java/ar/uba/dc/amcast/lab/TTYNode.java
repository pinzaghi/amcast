package ar.uba.dc.amcast.lab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.URingPaxosConsensus;
import ar.uba.dc.amcast.api.AmcastNode;
import ar.uba.dc.amcast.api.AmcastServerCommunication;
import ar.uba.dc.amcast.api.AmcastWatcher;
import ar.uba.dc.amcast.basecast.BasecastNode;
import ar.uba.dc.amcast.communication.ZookeeperUtils;
import ar.uba.dc.amcast.communication.io.ServerCommunication;
import ar.uba.dc.amcast.treecast.OverlayTreeNode;
import ar.uba.dc.amcast.treecast.TreecastNode;



public class TTYNode {
	
	private static final Logger LOGGER = Logger.getLogger( TTYNode.class.getName() );
	
	/**
	 * Thread that reads values from the standard input and proposes it
	 */
	private static class StdinParser implements Runnable  {
		
		AmcastNode node;
		AmcastWatcher watcher;
		
		public void register(AmcastNode node, AmcastWatcher watcher) {
			this.node = node;
			this.watcher = watcher;
		}
		
		public void run() {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String s;
	
			try {
				while ((s = in.readLine()) != null && s.length() != 0) {

					if(s.startsWith("info")) {

						continue;
					}
					
				}
				
				if(watcher != null) {
					watcher.close();
				}
				
				node.stop();				
				in.close();
				System.exit(0);
				
			} catch (IOException e) {
			}
			
		}

	}
	
	/*
	 * Example: ./scripts/ttynode.sh group=1 replica=1 zoohost=127.0.0.1:2181 watcher=writer overlaytree=simple
	 */
	public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException, KeeperException {
		
		Map<String, String> arguments = new LinkedHashMap<String, String>();
		
		for(int i = 0; i < args.length; i++) {
			String[] data = args[i].split("=");
			arguments.put(data[0], data[1]);
		}
		
		int groupID = Integer.parseInt(arguments.get("group"));
		int nodeID = Integer.parseInt(arguments.get("replica"));
		String zoo_host = String.valueOf(arguments.get("zoohost"));
		
		String watcher_arg = "";
		if(arguments.containsKey("watcher")) {
			watcher_arg = String.valueOf(arguments.get("watcher"));
		}
		
		Integer proposal_batch = null;
		if(arguments.containsKey("proposalbatch")) {
			proposal_batch = Integer.valueOf(arguments.get("proposalbatch"));
		}
		
		String overlaytree_arg = "";
		if(arguments.containsKey("overlaytree")) {
			LOGGER.log(Level.INFO, "TTYNode g:{0} r:{1} loading TreecastNode", new Object[] {groupID,nodeID});
			overlaytree_arg = String.valueOf(arguments.get("overlaytree"));
		}
		
		AmcastServerCommunication communication = new ServerCommunication(zoo_host);
		
		AmcastWatcher watcher = null;
		if(watcher_arg.equals("writer")) {
			watcher = new WriterWatcher(System.getProperty("user.home")+"/amcast_delivered_n"+nodeID+"_g"+groupID+".log");
		}
		
		ZookeeperUtils.configZookeeper(groupID, zoo_host);
		
		StdinParser stdin = new StdinParser();	
		
		URingPaxosConsensus consensus = new URingPaxosConsensus(zoo_host);

		AmcastNode node = null;

		if(arguments.containsKey("overlaytree")) {
			OverlayTreeNode<Integer> tree = OverlayTreeNode.getSampleTree(overlaytree_arg);
			
			node = new TreecastNode(tree, consensus, communication, nodeID, groupID, proposal_batch, watcher);
		}else {
			node = new BasecastNode(consensus, communication, nodeID, groupID, proposal_batch, watcher);
		}
		
		node.start();		
		
		stdin.register(node, watcher);

		Thread node_t = new Thread(stdin);
		node_t.setName("StdinNode");
		node_t.start();
		
		node_t.join();

	}
		


}
