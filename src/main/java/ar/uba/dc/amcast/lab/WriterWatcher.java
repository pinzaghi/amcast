package ar.uba.dc.amcast.lab;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import ar.uba.dc.amcast.api.AmcastWatcher;
import ar.uba.dc.amcast.message.Message;

public class WriterWatcher implements AmcastWatcher {
	
	PrintWriter fileWriter;

	long start_time;
	
	public WriterWatcher(String filepath) {

		try {
			fileWriter = new PrintWriter(filepath, "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void adeliver(Message message) {
		String s = null;
		try {
			System.out.println(message);
			s = new String(message.getValue(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		fileWriter.println(s);
		fileWriter.flush();
	}
	
	public void close() {
		fileWriter.close();
	}

}
