package ar.uba.dc.amcast.lab;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.zookeeper.KeeperException;


public class TCPServer implements Runnable {
	
	private final InetSocketAddress serverAddress;

	public TCPServer(int port) throws IOException {
		serverAddress = new InetSocketAddress("localhost", port);
	}

	
	public void run() {
		
		try {
			ServerSocket serverSocket = new ServerSocket(serverAddress.getPort());
			
			Socket clientSocket = serverSocket.accept();
			clientSocket.setTcpNoDelay(true);
			
			//Message m = new Message(MessageType.ClientStart, null, null, null, null, "123456");
			
			OutputStream os = null;
			InputStream is = null;
			DataInputStream in = null;
			
			try {
				os = clientSocket.getOutputStream();
				is = clientSocket.getInputStream();
				in = new DataInputStream(is);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			int count = 0;
			
			while(true){
				
				try {
					
					long start_time = System.nanoTime();
					
					//final byte[] receiveSizeBytes = new byte[4];
					//in.readFully(receiveSizeBytes);
		            //final int receiveSize = ByteBuffer.wrap(receiveSizeBytes).getInt();
					//int receiveSize = in.readInt();
					int receiveSize = is.read();
					final byte[] receiveBytes = new byte[receiveSize];
					is.read(receiveBytes);
		            //in.readFully(receiveBytes);
		            
		            long end_time = System.nanoTime();
					double difference = (end_time - start_time) / 1e6;
					System.out.println("Took "+difference+" ms");
		            System.out.println("Pong!");
					
					
		            start_time = System.nanoTime();
	    			//final byte[] sendBytes = Message.toWire(m);
		            byte[] sendBytes = new byte[10];
					//os.write(ByteBuffer.allocate(4).putInt(sendBytes.length).array());
		            os.write(sendBytes.length);
					os.write(sendBytes);
					
					end_time = System.nanoTime();
					difference = (end_time - start_time) / 1e6;

					System.out.println("Took "+difference+" ms");
					System.out.println("");
					
					
					count++;
					
	            } catch (IOException e1) {
					e1.printStackTrace();
				}
			
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	
	}
	
	
	public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException, KeeperException {
		
		TCPServer p = new TCPServer(2000);

		System.out.println("Starting server");
		Thread node_t = new Thread(p);
		node_t.setName("StdinNode");
		node_t.start();
		
		node_t.join();

	}
		


}
