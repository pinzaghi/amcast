package ar.uba.dc.amcast.lab;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.Client;
import ar.uba.dc.amcast.api.AmcastClientCommunication;
import ar.uba.dc.amcast.api.AmcastDeliveredCallback;
import ar.uba.dc.amcast.communication.io.ClientCommunication;
import ar.uba.dc.amcast.message.Result;
import ar.uba.dc.amcast.treecast.OverlayTreeNode;

public class ThroughputExperiment implements AmcastDeliveredCallback {
	
	protected static final Logger LOGGER = Logger.getLogger( ThroughputExperiment.class.getName() );

	Semaphore sendingWindow;
	public int counter = 0;
	
	public synchronized void messageDelivered(String messageId) {
		sendingWindow.release();
		counter++;
	}
	
	public synchronized int getCounter() {
		return counter;
	}
	
	public synchronized void resetCounter() {
		counter = 0;
	}
		
	public static void main(String[] args) {
		
		/*
		 * Example: ./scripts/exp_throughput.sh client=1234 groups=1,2 destinations=1:1,2 zoohost=127.0.0.1:2181 sendwindow=200
		 */
		
		try {

			Map<String, String> arguments = new LinkedHashMap<String, String>();
			
			for(int i = 0; i < args.length; i++) {
				String[] data = args[i].split("=");
				arguments.put(data[0], data[1]);
			}

			Integer clientId = Integer.parseInt(arguments.get("client"));
			String groups_arg = String.valueOf(arguments.get("groups"));
			String messageDestinations = String.valueOf(arguments.get("destinations"));
			String zoo_host = String.valueOf(arguments.get("zoohost"));
			Integer windowSize = Integer.parseInt(arguments.get("sendwindow"));
			String overlaytree_arg = "";
			if(arguments.containsKey("overlaytree")) {
				overlaytree_arg = String.valueOf(arguments.get("overlaytree"));
			}
			
			Random rand;
			// Using a seed in concurrent environments has a big impact on performance
			if(arguments.containsKey("seed")) {
				Integer seed = Integer.parseInt(arguments.get("seed"));
				rand = new Random(seed);
			}else {
				rand = new Random();
			}
			
			ThroughputExperiment dc = new ThroughputExperiment();
			dc.sendingWindow = new Semaphore(windowSize);
			
			AmcastClientCommunication communication = new ClientCommunication(zoo_host);

			Client client = null;
			
			if(!overlaytree_arg.isEmpty()) {
				OverlayTreeNode<Integer> tree = OverlayTreeNode.getSampleTree(overlaytree_arg);
				
				client = new Client(clientId, communication, tree);
			}else {
				String[] groups_array = groups_arg.split(",");
				LinkedHashSet<Integer> groups = new LinkedHashSet<Integer>();
				for(String group_str : groups_array) {
					Integer groupId = Integer.valueOf(group_str);
					groups.add(groupId);
				}
				client = new Client(clientId, communication, groups);
			}
			
			client.connectToLocalReplicas(1);
			
			LinkedList<LinkedHashSet<Integer>> destinations = new LinkedList<LinkedHashSet<Integer>>();
			
			String[] dsts_str = messageDestinations.split(":");
			for(String dst_str : dsts_str) {
				
				LinkedHashSet<Integer> dst_list = new LinkedHashSet<Integer>();
				
				String[] dst_number = dst_str.split(",");
				for(String dst_number_str : dst_number) {
					dst_list.add(Integer.valueOf(dst_number_str));
				}
				
				destinations.add(dst_list);
				
			}
			
			int i = 0;
			
			long start_time = System.nanoTime();
			Result result;
			
			byte[] msg = new byte[64];
			
			while(true) {
				LinkedHashSet<Integer> randomDst = destinations.get(rand.nextInt(destinations.size()));
				
				dc.sendingWindow.acquire();
				result = client.amulticast(msg, randomDst, dc);

				if(!result.isSuccessful()) {
					System.out.println("Not possible to send");
					System.exit(1);
				}

				double delta_t = (System.nanoTime() - start_time) / 1e6;
				
				if(delta_t >= 3000) {
					i = dc.getCounter();
					dc.resetCounter();
					
					System.out.println((i*1000/delta_t));
					
					LOGGER.log(Level.FINE, "{0}", new Object[] {delta_t});
					
					start_time = System.nanoTime();
				}

			}
		
		
		
		} catch (KeeperException | InterruptedException | IOException e) {
			e.printStackTrace();
        	System.exit(1);
		}
		
		
		System.exit(0);

	}

	

}
