package ar.uba.dc.amcast.lab;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;

import org.apache.zookeeper.KeeperException;

public class TCPClient implements Runnable {
	
	private InetSocketAddress addr;
	private CountDownLatch latch;
	private Socket socket;
	
	public TCPClient(int serverport) throws IOException {
		super();
		this.addr = new InetSocketAddress("localhost", serverport);
		socket = new Socket();
		socket.connect(this.addr);
		socket.setTcpNoDelay(true);
	}

	@Override
	public void run() {
		int count = 0;
		
		OutputStream os = null;
		InputStream is = null;
		DataInputStream in = null;
		
		try {
			os = socket.getOutputStream();
			is = socket.getInputStream();
			in = new DataInputStream(is);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		while(true){
			
			System.out.println("Ping "+count);
			
			//Message m = new Message(MessageType.ClientStart, null, null, null, null, "123456");
	        
            try {
    			//final byte[] sendBytes = Message.toWire(m);
            	long start_time = System.nanoTime();
            	byte[] sendBytes = new byte[10];
				//os.write(ByteBuffer.allocate(4).putInt(sendBytes.length).array());
				os.write(sendBytes.length);
				os.write(sendBytes);
				
				long end_time = System.nanoTime();
				double difference = (end_time - start_time) / 1e6;
				System.out.println("Took "+difference+" ms");
				
				start_time = System.nanoTime();
				//final byte[] receiveSizeBytes = new byte[4];
				//in.readFully(receiveSizeBytes);
				//final int receiveSize = ByteBuffer.wrap(receiveSizeBytes).getInt();
				//int receiveSize = in.readInt();
				int receiveSize = is.read();

	            final byte[] receiveBytes = new byte[receiveSize];
	            //in.readFully(receiveBytes);
	            is.read(receiveBytes);
	            
	            end_time = System.nanoTime();
				difference = (end_time - start_time) / 1e6;
				System.out.println("Took "+difference+" ms");
				System.out.println("");
				
				
            } catch (IOException e1) {
				e1.printStackTrace();
			}
			
	        count++;
	        
	        
	        
		}
		
		
	}

	
	public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException, KeeperException {
		
		TCPClient p = new TCPClient(2000);

		Thread node_t = new Thread(p);
		node_t.setName("Client");
		node_t.start();
		
		node_t.join();

	}

}
