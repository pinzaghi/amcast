package ar.uba.dc.amcast.lab;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.Client;
import ar.uba.dc.amcast.basecast.BasecastNode;
import ar.uba.dc.amcast.communication.io.ClientCommunication;
import ar.uba.dc.amcast.treecast.OverlayTreeNode;
import ar.uba.dc.amcast.treecast.TreecastNode;

public class LatencyExperiment {
	
		// 1 client sending global messages to <global_dst> groups
		// ./scripts/exp_latency.sh <dsts> <zoo_host>
		// ./scripts/exp_latency.sh destinations=1,2,3,4 zoohost=192.168.3.90:2181
		public static void main(String[] args) throws IOException, KeeperException, InterruptedException {
			
			Map<String, String> arguments = new LinkedHashMap<String, String>();
			
			for(int i = 0; i < args.length; i++) {
				String[] data = args[i].split("=");
				arguments.put(data[0], data[1]);
			}
			
			String groups_arg = "";
			if(arguments.containsKey("groups")) {
				groups_arg = String.valueOf(arguments.get("groups"));
			}
			
			String destinations_arg = String.valueOf(arguments.get("destinations"));
			String zoo_host = String.valueOf(arguments.get("zoohost"));
			
			Integer message_limit = 0;
			if(arguments.containsKey("messagelimit")) {
				message_limit = Integer.valueOf(arguments.get("messagelimit"));
			}
			
			Integer client_id = 1;
			if(arguments.containsKey("client")) {
				client_id = Integer.valueOf(arguments.get("client"));
			}
			
			String overlaytree_arg = "";
			if(arguments.containsKey("overlaytree")) {
				overlaytree_arg = String.valueOf(arguments.get("overlaytree"));
			}
			
			String messageDestinations = String.valueOf(arguments.get("destinations"));
			
			Random rand;
			// Using a seed in concurrent environments has a big impact on performance
			if(arguments.containsKey("seed")) {
				Integer seed = Integer.parseInt(arguments.get("seed"));
				rand = new Random(seed);
			}else {
				rand = new Random();
			}
			
			LinkedList<LinkedHashSet<Integer>> destinations = new LinkedList<LinkedHashSet<Integer>>();
			
			String[] dsts_str = messageDestinations.split(":");
			for(String dst_str : dsts_str) {
				
				LinkedHashSet<Integer> dst_list = new LinkedHashSet<Integer>();
				
				String[] dst_number = dst_str.split(",");
				for(String dst_number_str : dst_number) {
					dst_list.add(Integer.valueOf(dst_number_str));
				}
				
				destinations.add(dst_list);
				
			}
			
			launch_client(zoo_host, destinations, client_id, overlaytree_arg, groups_arg, message_limit, rand);
			
			System.exit(0);

		}
		
		public static void launch_client(String zoo_host, LinkedList<LinkedHashSet<Integer>> destinations, int client_id, String overlaytree, String groups_arg, Integer message_limit, Random rand) throws KeeperException, IOException, InterruptedException {
			//NIOClientCommunication communication = new NIOClientCommunication(zoo_host);
			ClientCommunication communication = new ClientCommunication(zoo_host);
			Client client = null;
					
			if(!overlaytree.isEmpty()) {
				OverlayTreeNode<Integer> tree = OverlayTreeNode.getSampleTree(overlaytree);
				
				client = new Client(client_id, communication, tree);
			}else {
				String[] groups_array = groups_arg.split(",");
				LinkedHashSet<Integer> groups = new LinkedHashSet<Integer>();
				for(String group_str : groups_array) {
					Integer groupId = Integer.valueOf(group_str);
					groups.add(groupId);
				}
				client = new Client(client_id, communication, groups);
			}
			
			client.connectToLocalReplicas(1);
			
			int msg_count = 0;
			double difference_sum = 0;
			double latency_sum = 0;
			
			while(true) {
				LinkedHashSet<Integer> randomDst = destinations.get(rand.nextInt(destinations.size()));
				
				byte[] msg = new byte[64];
				
				//System.out.println("Sending message "+msg+" to "+destinations);
				long start_time = System.nanoTime();
				client.amulticast(msg,randomDst);
				long end_time = System.nanoTime();

				msg_count++;
				
				if(message_limit > 0 && msg_count >= message_limit) {
					System.exit(0);
				}
				
				double delta_t = (end_time - start_time) / 1e6; // millis
				difference_sum = difference_sum + delta_t;
				latency_sum = latency_sum + delta_t;
				
				if(latency_sum >= 3000) {
					//System.out.println((difference_sum/msg_count));
					System.out.println(delta_t);
					latency_sum = 0;
				}
			}
			
		}

}
