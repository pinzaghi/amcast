package ar.uba.dc.amcast;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.api.AmcastNode;
import ar.uba.dc.amcast.api.AmcastServerCommunication;
import ar.uba.dc.amcast.api.AmcastWatcher;
import ar.uba.dc.amcast.api.Consensus;
import ar.uba.dc.amcast.basecast.BasecastNode;
import ar.uba.dc.amcast.communication.ZookeeperUtils;
import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.MessageType;

public abstract class Node implements AmcastNode {
	
	protected static final Logger LOGGER = Logger.getLogger( Node.class.getName() );
	
	protected Integer nodeId;
	protected Integer groupId;
	
	protected Consensus consensus;
	protected AmcastServerCommunication communicationManager;
	protected ConsensusWorker decisionWatcher;
	protected Boolean running;
	
	protected AmcastWatcher watcher;
	
	protected SocketAddress coordinatorAddress;
	
	protected Map<String, SocketAddress> messageOwnerSocket;
	protected Map<Integer, List<InetSocketAddress>> groupAddresses;
	
	public Node(Consensus consensus, AmcastServerCommunication network, Integer nodeId, Integer groupId, AmcastWatcher watcher, Integer proposalBatchSize) {
		this.nodeId = nodeId;
		this.groupId = groupId;
		
		this.consensus = consensus;
		this.running = false;
		
		this.watcher = watcher;
		this.decisionWatcher = new ConsensusWorker(consensus, this, proposalBatchSize);
		this.communicationManager = network;
		
		this.messageOwnerSocket = new ConcurrentHashMap<String, SocketAddress>();
		this.groupAddresses = new ConcurrentHashMap<Integer, List<InetSocketAddress>>();
		
		this.consensus.register(this);
		this.communicationManager.register(this);
	}
	
	public Consensus getConsensusInterface() {
		return consensus;
	}

	public AmcastServerCommunication getCommunicationInterface() {
		return communicationManager;
	}
	
	protected void sendMessageToCoordinator(Message m) {
		try {
			if(coordinatorAddress == null) {
				coordinatorAddress = ZookeeperUtils.getNodeSocketAddress(communicationManager.getZooKeeperHost(),groupId,getCoordinatorID());
			}
			communicationManager.send(m,coordinatorAddress);
		} catch (IOException | KeeperException | InterruptedException e) {
			StringWriter sw = new StringWriter();
	    	PrintWriter pw = new PrintWriter(sw);
	    	e.printStackTrace(pw);
	    	
	    	LOGGER.log(Level.SEVERE, "Exception {0} {1}", new Object[] {e, sw.toString()});
		}
	}
	
	public void confirmDeliveryToOwner(Message m) {
    	// Inform original sender that the protocol finished
    	if(messageOwnerKnown(m)) {
    		
    		//ServerCommunication.log( this, Level.FINE, m.getCacheId() + " serverComm.confirmDeliveryToClient ");
    		
    		Message controlMessage = new Message(MessageType.ClientMessageDelivered, null, groupId, null, null, m.getCacheId());
    		try {
				communicationManager.send(controlMessage,getOwnerSocketAddress(m));
			} catch (IOException e) {
				StringWriter sw = new StringWriter();
		    	PrintWriter pw = new PrintWriter(sw);
		    	e.printStackTrace(pw);
		    	
		    	LOGGER.log(Level.SEVERE, "Exception {0} {1}", new Object[] {e, sw.toString()});
			}

    		messageOwnerRemove(m);

    	}else {
    		//ServerCommunication.log(this, Level.FINE, "ADELIVER message "+ m + " aborted, not registered owner");
    	}
	}
	
	public List<InetSocketAddress> getMembersAddress(int groupId){
    	
    	List<InetSocketAddress> addrs = null;
    	
    	if(!groupAddresses.containsKey(groupId)) {
	    	try {
	    		addrs = ZookeeperUtils.getMembersAddress(groupId, this, communicationManager.getZooKeeperHost());
	    		groupAddresses.put(groupId, addrs);
	    	} catch (Exception e) {
				//ServerCommunication.log(this, Level.SEVERE, "Exception ocurred on getMembersAddress "+e);
			}
    	
    	}else {
    		addrs = groupAddresses.get(groupId);
    	}
    	
    	return addrs;
    	
    }
	
	protected void sendMessageToGroup(Message m, Integer groupId) {
		List<InetSocketAddress> addrs = getMembersAddress(groupId);
		//ServerCommunication.log(this, Level.FINER, "sending to group "+groupId+" with addrs "+addrs);
		for(InetSocketAddress addr : addrs) {
			try {
				communicationManager.send(m,addr);
			} catch (IOException e) {
				StringWriter sw = new StringWriter();
		    	PrintWriter pw = new PrintWriter(sw);
		    	e.printStackTrace(pw);
		    	
		    	LOGGER.log(Level.SEVERE, "Exception {0} {1}", new Object[] {e, sw.toString()});
			}
		}
	}
	
	public Boolean messageOwnerKnown(Message m) {
		return messageOwnerSocket.containsKey(m.getCacheId());
	}
	
    public void messageOwnerRemove(Message m) {
    	messageOwnerSocket.remove(m.getCacheId());
    }
    
	public SocketAddress getOwnerSocketAddress(Message m) {
		return messageOwnerSocket.get(m.getCacheId());
	}

	public synchronized Boolean isRunning() {
	    return running;
	}

	public String toString() {
		return "Node #"+getNodeId()+" G#"+getGroupId()+"";
	}
	
	public void proposeMessages(List<Message> messages) {
		LOGGER.log(Level.FINER, "Proposing {0} time {1}", new Object[] {messages,System.nanoTime()});
		decisionWatcher.propose(messages);
	}
	
	public synchronized void start() throws IOException, KeeperException, InterruptedException {
		
		consensus.start();
		
		Thread tcm = new Thread(communicationManager);
		tcm.setName("Basecast comm");
		tcm.start();

		Thread tdw = new Thread(decisionWatcher);
		tdw.setName("Basecast decisionwatch");
		tdw.start();
		
		running = true;
		
	}
	
	public synchronized void stop() throws IOException {
		
		//BasecastNode.log( this, Level.FINER, "stopping process");
		
		consensus.stop();
		communicationManager.stop();
		decisionWatcher.stop();
		
		running = false;

	}
	
	public static void log(BasecastNode p, Level level, String log) {
		LOGGER.log( level, p.toString()+" BaseCast: "+log+", timestamp "+System.nanoTime());	
	}

	public Integer getNodeId() {
		return nodeId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public boolean isCoordinator() {
		return consensus.isCoordinator(this);
	}
	
	public int getCoordinatorID() {
		return consensus.getCoordinatorID(this);
	}
	
	

}
