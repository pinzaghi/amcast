package ar.uba.dc.amcast;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.zookeeper.KeeperException;

import ar.uba.dc.amcast.api.AmcastClient;
import ar.uba.dc.amcast.api.AmcastClientCommunication;
import ar.uba.dc.amcast.api.AmcastDeliveredCallback;
import ar.uba.dc.amcast.message.Message;
import ar.uba.dc.amcast.message.MessageType;
import ar.uba.dc.amcast.message.Result;
import ar.uba.dc.amcast.message.ResultType;
import ar.uba.dc.amcast.treecast.OverlayTreeNode;


public class Client implements AmcastClient {
	
	private static final Logger LOGGER = Logger.getLogger( Client.class.getName() );
	
	private Integer clientId;	
	private AmcastClientCommunication comm;
	private Map<String, AmcastDeliveredCallback> messagesSentAsync;
	private Map<String, CountDownLatch> messagesSentBlocking;
	private List<Message> timeoutedMessages;
	// Only used in Basecast
	private Set<Integer> groups;
	// Only used with Treecast
	private OverlayTreeNode<Integer> groupsOverlayTree;
	
	public Client(Integer clientId, AmcastClientCommunication comm, Set<Integer> groups) throws IOException, KeeperException, InterruptedException {
		this.groups = groups;
		init(clientId, comm);
	}
	
	public Client(Integer clientId, AmcastClientCommunication comm, OverlayTreeNode<Integer> overlayTree) throws IOException, KeeperException, InterruptedException {
		this.groupsOverlayTree = overlayTree;
		init(clientId, comm);
	}
	
	private void init(Integer clientId, AmcastClientCommunication comm) {
		this.clientId = clientId;
		this.comm = comm;
		this.messagesSentAsync = new ConcurrentHashMap<String,AmcastDeliveredCallback>();
		this.messagesSentBlocking = new ConcurrentHashMap<String,CountDownLatch>();
		this.comm.register(this);
		this.timeoutedMessages = new LinkedList<Message>();
		
		Thread tcm = new Thread(this.comm);
		tcm.start();
	}

	public String generateId(byte[] m) {
		return UUID.randomUUID().toString()+":"+clientId;
	}
	
	public Result amulticast(byte[] message, Set<Integer> dst, AmcastDeliveredCallback callee) throws IOException {
		Boolean sendingIsPossible = comm.sendingIsPossible(dst);
		Result result = null;
		
		if(!sendingIsPossible) {
			result = new Result(ResultType.SendingNotPossible);
		}else {

			// We will wait for at least one node to confirm adeliver
			String messageId = generateId(message);
			result = new Result(ResultType.Success, messageId);

			messagesSentAsync.put(messageId, callee);

			// Send the message to the involved nodes
			Message controlMessage = new Message(MessageType.ClientStart,dst,null,null,message,messageId);
			
			sendStartMessage(controlMessage);

		}
		
		return result;
	}
	
	private void sendStartMessage(Message controlMessage) throws IOException {
		
		Integer dstGroup;
		
		// In a tree topology we send the message to the Lower Common Ancestor
		if(groupsOverlayTree != null) {
			OverlayTreeNode<Integer> lca = groupsOverlayTree.lowerCommonAncestor(controlMessage.getDestinations());
			LOGGER.log(Level.FINEST, "Sending start message to LCA {0}", new Object[] {lca});
			dstGroup = lca.data;
		// If we don't have a special topology, we send it to any of the dst group
		}else {
			dstGroup = controlMessage.getDestinations().iterator().next();
			LOGGER.log(Level.FINEST, "Sending start message to one of the dst groups: {0}", new Object[] {dstGroup});
		}
		
		comm.send(controlMessage, dstGroup);
		
	}

	public Boolean amulticast(byte[] message, Set<Integer> dst) throws InterruptedException, IOException {
		
		Boolean result = false;
		Boolean sendingIsPossible = comm.sendingIsPossible(dst);
		
		if(!sendingIsPossible) {
			//Client.log( this, Level.WARNING,  "Sending of message is not possible, client must be attached to at least one node of the destination group");
		}else {
			// We will wait for at least one node to confirm adeliver
			CountDownLatch latch = new CountDownLatch(1);
			String messageId = generateId(message);
			
			//Client.log( this, Level.FINE, messageId + " timestamp amulticast, "+System.nanoTime());
			
			messagesSentBlocking.put(messageId, latch);

			// Send the message to the involved nodes
			Message controlMessage = new Message(MessageType.ClientStart,dst,null,null,message,messageId);
			
			sendStartMessage(controlMessage);
		
			// Wait them to finish the amulticast
			//Client.log( this, Level.FINER, "Waiting for the nodes to finish the protocol");

			result = latch.await(10000, TimeUnit.MILLISECONDS);
			
			if(result) {
				//Client.log( this, Level.FINER, "Message "+message+" delivery confirmed.");
			}else {
				this.timeoutedMessages.add(controlMessage);
				Client.log( this, Level.SEVERE, "Message "+message+" delivery TIMEOUT!.");
			}

		}
		
		return result;
	}
	
	public void receiveFromCommunication(Message m) {
		
		Client.log( this, Level.FINE, "Message "+m+" received.");
		
		if(m.getType() == MessageType.ClientMessageDelivered) {

			if(messagesSentAsync.containsKey(m.getCacheId())) {
				
				messagesSentAsync.get(m.getCacheId()).messageDelivered(m.getCacheId());
				messagesSentAsync.remove(m.getCacheId());
				
			}else if(messagesSentBlocking.containsKey(m.getCacheId())) {
				
				messagesSentBlocking.get(m.getCacheId()).countDown();
				messagesSentBlocking.remove(m.getCacheId());
			}
			
		}else {
			//Client.log( this, Level.WARNING, "Received "+m+", not expected");
		}
	}
	
	public List<Message> getTimeoutedMessages() {
		return this.timeoutedMessages;
	}

	public Integer getClientId() {
		return clientId;
	}
	
	public String toString() {
		return "CLIENT #"+getClientId();
	}
	
	public static void log(Client c, Level level, String log) {
		LOGGER.log( level, c.toString()+" "+log+ " timestamp "+System.nanoTime());	
	}

	@Override
	public void connectToLocalReplicas(Integer replicaId)
			throws KeeperException, InterruptedException, IOException {
		
		if(groupsOverlayTree != null) {
			for(OverlayTreeNode<Integer> node : groupsOverlayTree) {
				Integer groupId = node.data;
				comm.registerNode(groupId, replicaId);
			}
			
		}else {
			for(Integer groupId : groups) {
				comm.registerNode(groupId, replicaId);
			}
		}
		
	}

}
